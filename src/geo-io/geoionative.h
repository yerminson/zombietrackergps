/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIONATIVE_H
#define GEOIONATIVE_H

#include <src/util/versionedstream.h>

#include "geoiobase.h"

// Subclass of GeoLoadBase for high performance private binary blob
class GeoLoadNative final : public GeoLoadBase
{
public:
    GeoLoadNative(GeoLoad& geoLoad);

    bool load(const QString& path) override;
    bool is(const QString& path) override;

    static const QString name;

private slots:
    void loadPos(qint64 pos) { reportRead(pos); }

private:
    bool openReader(QFile& file);

    VersionedStream readStream;
};

class GeoSaveNative final : public GeoSaveBase
{
public:
    GeoSaveNative(GeoSave& geoSave);

    bool save(const QString& path) override;

    static const QString name;

private slots:
    void saveItem(qint64 i) { reportWrite(i); }

private:
    bool openWriter(QFile& file);

    VersionedStream writeStream;
};

#endif // GEOIONATIVE_H


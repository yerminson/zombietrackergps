/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <marble/MarbleGlobal.h>

#include <src/util/resources.h>
#include <src/util/ui.h>
#include <src/util/cmdline.h>
#include "src/version.h"
#include "src/ui/windows/mainwindow.h"

void usage(const char* prog)
{
    const char* base = basename(prog);

    printf("Usage: %s [args...]\n"
           "   --help           Print usage information and exit.\n"
           "   --version        Print program version and exit.\n"
           "   --pubkey         Dump PGP/GPG public key to stdout and exit.\n"
           "   --conf FILE      Load session from .conf file named FILE\n"
           "   --desktop NAME   Sets the XDG_CURRENT_DESKTOP environment variable to NAME.  This\n"
           "                    can be useful when running the program under sudo, for example as\n"
           "                    in:\n"
           "                       sudo -H -u user %s --desktop KDE\n"
           "                    This will allow KDE to use a native theme under sudo.\n",
           base, base);
}

namespace {

const char* initialSettingsFile = "";

void processArgs(int argc, char *argv[])
{
    for (int arg = 1; arg < argc; ++arg) {
        if (Util::ProcessArgs(appname, version, argc, argv, arg)) {
        } else if (strcmp(argv[arg], "--conf") == 0 && arg < (argc-1)) {
            initialSettingsFile = argv[++arg];
        }
    }
}

// Arguments we can only process after the application is created.
void processArgsPostApp(int argc, char *argv[])
{
    for (int arg = 1; arg < argc; ++arg)
        Util::ProcessArgsPostApp(argv[arg]);
}

// Allow conversion to QVariant
QDataStream& operator<<(QDataStream& out, const Marble::MapQuality& mc)
{
    return out << std::underlying_type<Marble::MapQuality>::type(mc);
}

// Allow conversion to QVariant
QDataStream& operator>>(QDataStream& in, Marble::MapQuality& mc)
{
    return in >> (std::underlying_type<Marble::MapQuality>::type&)(mc);
}

void registerTypes()
{
    qRegisterMetaType<TrackType>("TrackType");

    // Allow passing these things between threads
    qRegisterMetaTypeStreamOperators<Marble::MapQuality>("Marble::MapQuality");
    qRegisterMetaTypeStreamOperators<PaneClass>("PaneClass");
    qRegisterMetaTypeStreamOperators<TrackType>("TrackType");
    qRegisterMetaTypeStreamOperators<GeoFormat>("GeoFormat");
    qRegisterMetaTypeStreamOperators<Qt::SortOrder>("Qt::SortOrder");
    qRegisterMetaTypeStreamOperators<QVector<QVariant>>("QVector<QVariant>");
}

void setupResources()
{
    for (const QString file : { "art.rcc",
                                 Util::IsLightTheme() ? "art-light.rcc" : "art-dark.rcc",
                                 Util::IsLightTheme() ? "icons-light.rcc" : "icons-dark.rcc" } )
        if (!Util::LoadResourceFile(file))
            exit(5);
}

} // end anonymous namespace

QDataStream& operator<<(QDataStream& out, const Qt::SortOrder& s) {
    return out << int(s);
}

QDataStream& operator>>(QDataStream& in, Qt::SortOrder& s) {
    return in >> (int &)(s);
}

int main(int argc, char *argv[])
{
    QApplication::setApplicationName(appname);
    QApplication::setApplicationDisplayName(apptitle);
    QApplication::setApplicationVersion(version);

    processArgs(argc, argv);
    registerTypes();

    QApplication a(argc, argv);
    setupResources(); // do after application is created, but before window is.
    processArgsPostApp(argc, argv);

    MainWindow w(initialSettingsFile);
    w.show();

    return a.exec();
}

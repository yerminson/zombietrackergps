/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSGARMIN_H
#define GPSGARMIN_H

#include <functional>
#include "src/dev-io/gpsdevice.h"

class XmlStreamReader;

class GpsGarmin : public GpsDevice
{
public:
    GpsGarmin(const QString& mount);

    QString make() const override;               // return device make
    QString model() const override;              // return model name
    bool    isMounted() const override;          // true if device is mounted
    QString file(Data, Transfer) const override; // device filenames

    static bool is(const QString& mount);

protected:
    void parse();
    void parseDevice(XmlStreamReader&);
    void parseModel(XmlStreamReader&);
    void parseMassStorageMode(XmlStreamReader&);
    void parseDataType(XmlStreamReader&);
    void parseFile(XmlStreamReader&);
    void parseLocation(XmlStreamReader&);

    QString garminDeviceFile() const; // find garmin device file, if mounted
    static QString garminDeviceFile(const QString& mount);
    static bool isMounted(const QString& mount);

    QString m_model;             // device model
    QString m_currentGpx;        // GPX output from unit

    struct {
        void clearDataType() {
            name.clear();
            transferDirection.clear();
            path.clear();
            baseName.clear();
            fileExtension.clear();
        }

        void clear() { clearDataType(); }

        QString name;
        QString transferDirection;
        QString path;
        QString baseName;
        QString fileExtension;
    } parseInfo;
};

#endif // GPSGARMIN_H

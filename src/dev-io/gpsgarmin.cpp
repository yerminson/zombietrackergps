/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QStorageInfo>
#include <QDir>
#include <QFile>
#include <QStringList>

#include "src/util/xmlstreamreader.h"
#include "gpsgarmin.h"

GpsGarmin::GpsGarmin(const QString& mount) :
    GpsDevice(mount)
{
    parse();
}

void GpsGarmin::parse()
{
    QFile deviceFile(garminDeviceFile());
    if (deviceFile.fileName().isEmpty())
        return;

    if (!deviceFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    XmlStreamReader xml(&deviceFile);

    if (xml.error() != QXmlStreamReader::NoError)
        return;

    xml.parseKeys([this](XmlStreamReader& xml) {
        if (xml.name() == "Device") parseDevice(xml);
        else xml.skipCurrentElement();
    });

    parseInfo.clear();
}

void GpsGarmin::parseDevice(XmlStreamReader& xml)
{
    xml.parseKeys([this](XmlStreamReader& xml) {
        if      (xml.name() == "Model")           parseModel(xml);
        else if (xml.name() == "MassStorageMode") parseMassStorageMode(xml);
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseModel(XmlStreamReader& xml)
{
    xml.parseKeys([this](XmlStreamReader& xml) {
        if   (xml.name() == "Description") m_model = xml.readElementText();
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseMassStorageMode(XmlStreamReader& xml)
{
    xml.parseKeys([this](XmlStreamReader& xml) {
        if   (xml.name() == "DataType") parseDataType(xml);
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseDataType(XmlStreamReader& xml)
{
    parseInfo.clearDataType();

    xml.parseKeys([this](XmlStreamReader& xml) {
        if      (xml.name() == "Name") parseInfo.name = xml.readElementText();
        else if (xml.name() == "File") parseFile(xml);
        else xml.skipCurrentElement();
    });

    if (parseInfo.transferDirection == "OutputFromUnit" &&
        !parseInfo.baseName.isEmpty() &&
        parseInfo.name == "GPSData" &&
        (parseInfo.fileExtension.toLower() == "gpx" || parseInfo.fileExtension.toLower() == "tcx")) {
        m_currentGpx = parseInfo.path + QDir::separator() + parseInfo.baseName + "." + parseInfo.fileExtension;
    }
}

void GpsGarmin::parseFile(XmlStreamReader& xml)
{
    xml.parseKeys([this](XmlStreamReader& xml) {
        if      (xml.name() == "Location")          parseLocation(xml);
        else if (xml.name() == "TransferDirection") parseInfo.transferDirection = xml.readElementText();
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseLocation(XmlStreamReader& xml)
{
    xml.parseKeys([&](XmlStreamReader& xml) {
        if      (xml.name() == "Path")          parseInfo.path = xml.readElementText();
        else if (xml.name() == "BaseName")      parseInfo.baseName = xml.readElementText();
        else if (xml.name() == "FileExtension") parseInfo.fileExtension = xml.readElementText();
        else xml.skipCurrentElement();
    });
}

QString GpsGarmin::make() const
{
    return "Garmin";
}

QString GpsGarmin::model() const
{
    return m_model;
}

bool GpsGarmin::isMounted(const QString& mount)
{
    const QStorageInfo mountInfo = QStorageInfo(mount);

    return mountInfo.isValid() &&
           mountInfo.isReady() &&
           QDir(mountInfo.rootPath()).canonicalPath() == QDir(mount).canonicalPath();
}

bool GpsGarmin::isMounted() const
{
    return isMounted(mountPoint());
}

QString GpsGarmin::garminDeviceFile(const QString& mount)
{
    if (!isMounted(mount))
        return QString();

    const QFile garminDevice(mount + QDir::separator() + "Garmin" + QDir::separator() + "GarminDevice.xml");

    if (garminDevice.exists())
        return garminDevice.fileName();

    return QString();
}

QString GpsGarmin::garminDeviceFile() const
{
    return garminDeviceFile(mountPoint());
}

bool GpsGarmin::is(const QString& mount)
{
    return !garminDeviceFile(mount).isEmpty();
}

QString GpsGarmin::file(Data data, Transfer transfer) const
{
    if (data == Data::GPS && transfer == Transfer::Output)
        return mountPoint() + QDir::separator() + m_currentGpx;

    return QString();
}

/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SELECTIONSUMMARY_H
#define SELECTIONSUMMARY_H

#include <QtGlobal>
#include <QItemSelection>

class TrackModel;
class PointModel;
class QItemSelectionModel;
class QAbstractProxyModel;

class SelectionSummary
{
public:
    SelectionSummary();

    void clear(int totalItems, int visibleItems);
    void update(const TrackModel&, const QAbstractProxyModel&, const QItemSelectionModel* = nullptr,
                const QItemSelection& selected = QItemSelection(),
                const QItemSelection& deselected = QItemSelection());
    void update(const PointModel&, const QAbstractProxyModel&, const QItemSelectionModel* = nullptr,
                const QItemSelection& selected = QItemSelection(),
                const QItemSelection& deselected = QItemSelection());

    void accumulate(const TrackModel&, const QModelIndex& idx, int sign);
    void accumulate(const PointModel&, const QModelIndex& idx, int sign);

    int    total;
    int    visible;
    int    selected;
    qreal  length;
    qreal  ascent;
    qreal  descent;
    qint64 duration;

private:
    template <class MODEL>
    void trackSelected(const MODEL& model, const QItemSelectionModel* selectionModel,
                       const QItemSelection& selected, const QItemSelection& deselected);
};

#endif // SELECTIONSUMMARY_H

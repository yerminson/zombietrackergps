/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PERSONITEM_H
#define PERSONITEM_H

#include <QString>

#include <src/core/treeitem.h>

class TagModel;
class CfgData;

class PersonItem final : public TreeItem
{
private:
    explicit PersonItem(const CfgData&, TreeItem *parent = 0);
    explicit PersonItem(const CfgData&, const TreeItem::ItemData&, TreeItem* parent = nullptr);

    void init(const TreeItem::ItemData& data = TreeItem::ItemData());

    QVariant data(int column, int role) const override;

    using TreeItem::setData;
    bool setData(int column, const QVariant &value, int role, bool& changed) override;

    int columnCount() const override;

    PersonItem* factory(const PersonItem::ItemData& data, TreeItem* parent) override;

    const CfgData& cfgData;

    friend class PersonModel;
};

#endif // PERSONITEM_H

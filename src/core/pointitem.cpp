/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDataStream>
#include <src/util/roles.h>
#include <src/util/math.h>
#include <src/util/units.h>
#include <src/util/versionedstream.h>

#include <src/core/modelmetadata.h>
#include "pointitem.h"
#include "pointmodel.h"
#include "cfgdata.h"

void PointItem::clear()
{
    *this = PointItem();
}

bool PointItem::hasExtData() const
{
    return hasAtemp()         ||
           hasWtemp()         ||
           hasDepth()         ||
           hasSpeed(Measured) ||
           hasHr()            ||
           hasCad()           ||
           hasPower(Measured) ||
           hasCourse()        ||
           hasBearing();
}

template <>
QVariant PointItem::data<QVariant>(ModelType mt, int row, int role, bool flt, const QDateTime& start, const PowerData& pd, const CfgData& cfgData) const
{
    static const constexpr qint64 mStonS = 1000000;  // mS to nS

    static const QVariant empty;

    if (!hasData(mt))
        return empty;

    QVariant value;

    switch (mt) {
    case PointModel::Index:     value = row;                     break;
    case PointModel::Time:      value = time();                  break;
    case PointModel::Elapsed:   value = mStonS * elapsed(start); break;
    case PointModel::Lon:       value = lon(flt);                break;
    case PointModel::Lat:       value = lat(flt);                break;
    case PointModel::Ele:       value = ele(flt);                break;
    case PointModel::Length:    value = length();                break;
    case PointModel::Distance:  value = distance();              break;
    case PointModel::Vert:      value = vert();                  break;
    case PointModel::Grade:     value = grade();                 break;
    case PointModel::Duration:  value = mStonS * duration();     break;
    case PointModel::Temp:      value = atemp();                 break;
    case PointModel::Depth:     value = depth();                 break;
    case PointModel::Speed:     value = speed();                 break;
    case PointModel::Accel:     value = accel();                 break;
    case PointModel::Hr:        value = hr();                    break;
    case PointModel::Cad:       value = cad();                   break;
    case PointModel::Power:     value = power(pd);               break;
    case PointModel::Course:    value = course();                break;
    case PointModel::Bearing:   value = bearing();               break;
    default: assert(0); break;
    }

    if (role == Qt::EditRole)
        return PointModel::mdUnits(mt, cfgData).to(value);

    return value;
}

template <>
qreal PointItem::data<qreal>(ModelType mt, int row, int role, bool flt, const QDateTime& start, const PowerData& pd, const CfgData& cfgData) const
{
    return data<QVariant>(mt, row, role, flt, start, pd, cfgData).toDouble();
}

void PointItem::clearData(ModelType mt)
{
    switch (mt) {
    case PointModel::Temp:  m_atemp = fNaN; return;
    case PointModel::Depth: m_depth = fNaN; return;
    case PointModel::Hr:    m_hr  = badHr;  return;
    case PointModel::Cad:   m_cad = badCad; return;
    default: return;
    }
}

bool PointItem::setData(ModelType mt, QVariant value, int role, bool& changed, const CfgData& cfgData)
{
    changed = true; // TODO: we could be smarter about this.

    if (role == Qt::EditRole) {
        value = PointModel::mdUnits(mt, cfgData).from(value);

        // Unset certain columns if set to zero.  (OK to check ints as doubles, for convenience)
        switch (mt) {
        case PointModel::Temp:  [[fallthrough]];
        case PointModel::Depth: [[fallthrough]];
        case PointModel::Hr:    [[fallthrough]];
        case PointModel::Cad:
            if (value.toDouble() == 0)
                clearData(mt);
            break;
        default:
            break;
        }
    }

    switch (mt) {
    case PointModel::Time:   m_time  = value.toDateTime(); break;
    case PointModel::Lon:    m_lon   = value.toDouble();   break;
    case PointModel::Lat:    m_lat   = value.toDouble();   break;
    case PointModel::Ele:    m_ele   = value.toDouble();   break;
    case PointModel::Temp:   m_atemp = value.toDouble();   break;
    case PointModel::Depth:  m_depth = value.toDouble();   break;
    case PointModel::Hr:     m_hr    = value.toInt();      break;
    case PointModel::Cad:    m_cad   = value.toInt();      break;
    default:
        assert(0);
        return false;
    }

    return true;
}

bool PointItem::hasData(ModelType mt) const
{
    switch (mt) {
    case PointModel::Index:     return true;
    case PointModel::Time:      return hasTime();
    case PointModel::Elapsed:   return hasElapsed();
    case PointModel::Lon:       return hasLoc();
    case PointModel::Lat:       return hasLoc();
    case PointModel::Ele:       return hasEle();
    case PointModel::Length:    return hasLength();
    case PointModel::Distance:  return hasDistance();
    case PointModel::Vert:      return hasVert();
    case PointModel::Grade:     return hasGrade();
    case PointModel::Duration:  return hasDuration();
    case PointModel::Temp:      return hasAtemp();
    case PointModel::Depth:     return hasDepth();
    case PointModel::Speed:     return hasSpeed(Either);
    case PointModel::Accel:     return hasAccel();
    case PointModel::Hr:        return hasHr();
    case PointModel::Cad:       return hasCad();
    case PointModel::Power:     return hasPower(Either);
    case PointModel::Course:    return hasCourse();
    case PointModel::Bearing:   return hasBearing();
    }

    assert(0);
    return false;
}

bool PointItem::operator==(const PointItem& other) const
{
    // Do not compare the filtered data!  We can change that per config settings.
    // No need to compare derived (calculated) values such as distance and acceleration.
    return
        hasTime()          == other.hasTime()          &&
        hasLoc()           == other.hasLoc()           &&
        hasEle()           == other.hasEle()           &&
        hasAtemp()         == other.hasAtemp()         &&
        hasWtemp()         == other.hasWtemp()         &&
        hasDepth()         == other.hasDepth()         &&
        hasSpeed(Measured) == other.hasSpeed(Measured) &&
        hasHr()            == other.hasHr()            &&
        hasCad()           == other.hasCad()           &&
        hasPower(Measured) == other.hasPower(Measured) &&
        hasCourse()        == other.hasCourse()        &&
        hasBearing()       == other.hasBearing()       &&

        (!hasTime()      || m_time == other.m_time) &&
        (!hasLoc()       || Math::almost_equal(m_lon , other.m_lon, 2)) &&
        (!hasLoc()       || Math::almost_equal(m_lat , other.m_lat, 2)) &&
        (!hasEle()       || Math::almost_equal(m_ele , other.m_ele, 2)) &&
        (!hasAtemp()     || Math::almost_equal(m_atemp, other.m_atemp, 2)) &&
        (!hasWtemp()     || Math::almost_equal(m_wtemp, other.m_wtemp, 2)) &&
        (!hasDepth()     || Math::almost_equal(m_depth, other.m_depth, 2)) &&
        (!hasSpeed(Measured) || Math::almost_equal(m_speed, other.m_speed, 2)) &&
        m_hr      == other.m_hr    &&  // int types can use exact comparison
        m_cad     == other.m_cad   &&  // ...
        (!hasPower(Measured) || Math::almost_equal(m_power, other.m_power, 2)) &&
        (!hasCourse()    || Math::almost_equal(m_course, other.m_course, 2)) &&
        (!hasBearing()   || Math::almost_equal(m_bearing, other.m_bearing, 2));
}

QDataStream &operator<<(QDataStream& stream, const PointItem& pt)
{
    // Accel, Distance, etc are computed from other values: no need to save/load
    return stream << pt.m_atemp
                  << pt.m_wtemp
                  << pt.m_depth
                  << pt.m_speed
                  << pt.m_hr
                  << pt.m_cad
                  << pt.m_power
                  << pt.m_course
                  << pt.m_bearing
                  << pt.m_time
                  << pt.m_lon
                  << pt.m_lat
                  << pt.m_ele;
}

QDataStream &operator>>(QDataStream& stream, PointItem& pt)
{
    // Accel, Distance, etc are computed from other values: no need to save/load
    return stream >> pt.m_atemp
                  >> pt.m_wtemp
                  >> pt.m_depth
                  >> pt.m_speed
                  >> pt.m_hr
                  >> pt.m_cad
                  >> pt.m_power
                  >> pt.m_course
                  >> pt.m_bearing
                  >> pt.m_time
                  >> pt.m_lon
                  >> pt.m_lat
                  >> pt.m_ele;
}

uint qHash(const PointItem& pt, uint seed)
{
    // Accel, Distance, etc are computed from other values: no need to hash
    if (pt.hasTime())                   seed = qHash(std::make_pair(seed, pt.time()));
    if (pt.hasLoc())                    seed = qHash(std::make_pair(seed, pt.lon(false)));
    if (pt.hasLoc())                    seed = qHash(std::make_pair(seed, pt.lat(false)));
    if (pt.hasEle())                    seed = qHash(std::make_pair(seed, pt.ele(false)));

    if (pt.hasAtemp())                  seed = qHash(std::make_pair(seed, pt.atemp()));
    if (pt.hasWtemp())                  seed = qHash(std::make_pair(seed, pt.wtemp()));
    if (pt.hasDepth())                  seed = qHash(std::make_pair(seed, pt.depth()));
    if (pt.hasSpeed(PointItem::Measured)) seed = qHash(std::make_pair(seed, pt.speed()));
    if (pt.hasHr())                     seed = qHash(std::make_pair(seed, pt.hr()));
    if (pt.hasCad())                    seed = qHash(std::make_pair(seed, pt.cad()));
    if (pt.hasPower(PointItem::Measured)) seed = qHash(std::make_pair(seed, pt.power()));
    if (pt.hasCourse())                 seed = qHash(std::make_pair(seed, pt.course()));
    if (pt.hasBearing())                seed = qHash(std::make_pair(seed, pt.bearing()));

    return seed;
}

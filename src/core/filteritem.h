/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CATEGORYITEM_H
#define CATEGORYITEM_H

#include <QScopedPointer>
#include <src/core/treeitem.h>
#include <src/core/query.h>

class FilterModel;

class FilterItem final : public TreeItem
{
private:
    friend class FilterModel;

    explicit FilterItem(const ItemData& data, TreeItem *parent = 0);
    explicit FilterItem(TreeItem *parent = 0);

    void init(const TreeItem::ItemData& data = TreeItem::ItemData());

    using TreeItem::data;
    QVariant data(int column, int role) const override;

    using TreeItem::setData;
    bool setData(int column, const QVariant &value, int role, bool& changed) override;

    int columnCount() const override;

    FilterItem* factory(const FilterItem::ItemData& data, TreeItem* parent) override;

    FilterItem(const FilterItem&) = delete;
    FilterItem& operator=(const FilterItem&) = delete;
};

#endif // CATEGORYITEM_H

/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QFileInfo>

#include <src/core/cfgdata.h>
#include <src/util/roles.h>
#include "personmodel.h"
#include "personitem.h"

PersonItem::PersonItem(const CfgData& cfgData, TreeItem* parent) :
    TreeItem(parent),
    cfgData(cfgData)
{
    init();
}

PersonItem::PersonItem(const CfgData& cfgData, const TreeItem::ItemData& data, TreeItem* parent) :
    TreeItem(parent),
    cfgData(cfgData)
{
    init(data);
}

void PersonItem::init(const TreeItem::ItemData& data)
{
    for (int t = 0; t < PersonModel::_Count; ++t)
        if (data.size() > t && data[t].isValid())
            setData(t, data[t], PersonModel::mdDataRole(t));
}

QVariant PersonItem::data(int column, int role) const
{
    if (parent() == nullptr)
        return TreeItem::data(column, role);

    const QVariant rawData = TreeItem::data(column, Util::RawDataRole);

    switch (role) {
    case Qt::TextAlignmentRole:
        return QVariant(PersonModel::mdAlignment(column));

    case Qt::DisplayRole:
    case Util::CopyRole:
        return PersonModel::mdUnits(column, cfgData)(rawData);

    case Qt::EditRole:
        return PersonModel::mdUnits(column, cfgData).to(rawData);
    }

    return TreeItem::data(column, role);
}

bool PersonItem::setData(int column, const QVariant& value, int role, bool &changed)
{
    if (role == Qt::EditRole) {
        role = PersonModel::mdDataRole(column);
        const QVariant unitVal = PersonModel::mdUnits(column, cfgData).from(value);
        return TreeItem::setData(column, unitVal, role, changed);
    }

    return TreeItem::setData(column, value, role, changed);
}

int PersonItem::columnCount() const
{
    return PersonModel::_Count;
}

PersonItem *PersonItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<PersonItem*>(parent) != nullptr);

    return new PersonItem(cfgData, data, parent);
}

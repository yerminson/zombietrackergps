/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/roles.h>
#include "filteritem.h"
#include "filtermodel.h"

FilterItem::FilterItem(const FilterItem::ItemData& data, TreeItem *parent) :
    TreeItem(data, parent)
{
    init(data);
}

FilterItem::FilterItem(TreeItem *parent) :
    TreeItem(parent)
{
}

void FilterItem::init(const TreeItem::ItemData& data)
{
    for (int t = 0; t < FilterModel::_Count; ++t)
        if (data.size() > t && data[t].isValid())
            setData(t, data[t], FilterModel::mdDataRole(t));
}

QVariant FilterItem::data(int column, int role) const
{
    if (parent() == nullptr)
        return TreeItem::data(column, role);

    const QVariant rawData = TreeItem::data(column, Util::RawDataRole);

    switch (role) {
    case Qt::TextAlignmentRole:
        return QVariant(FilterModel::mdAlignment(column));
    case Qt::DisplayRole:
    case Qt::EditRole:
    case Util::CopyRole:
        return rawData;
    }

    return TreeItem::data(column, role);
}

bool FilterItem::setData(int column, const QVariant &value, int role, bool &changed)
{
    if (role == Qt::EditRole)
        role = Util::RawDataRole;

    return TreeItem::setData(column, value, role, changed);
}

int FilterItem::columnCount() const
{
    return FilterModel::_Count;
}

FilterItem *FilterItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<FilterItem*>(parent) != nullptr);

    return new FilterItem(data, parent);
}

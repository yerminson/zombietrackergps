/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKDETAILFILTER_H
#define TRACKDETAILFILTER_H

#include <src/ui/filters/detailfilter.h>
#include "src/core/trackmodel.h"

class TrackDetailFilter : public DetailFilter
{
public:
    TrackDetailFilter(QObject *parent = nullptr);

private:
    enum {
        _First  = int(TrackModel::_Count),
        Meta = _First,
        Track,
        Geography,
        Points,
        Extent,
        Times,
        Speeds,
        Elevation,
        HighLow,
        Grade,
        Climb,
        Exercise,
        Cadence,
        Power,
        Temperature,
        HeartRate,
        _Count,
    };

    QString text(Header) const override;

    static const Line rootItem; // structure
};

#endif // TRACKDETAILFILTER_H

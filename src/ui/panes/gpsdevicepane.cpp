/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gpsdevicepane.h"
#include "ui_gpsdevicepane.h"

#include <src/util/icons.h>
#include "src/ui/windows/mainwindow.h"
#include "src/dev-io/gpsmodel.h"

GpsDevicePane::GpsDevicePane(MainWindow& mainWindow, bool contextMenus, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::GpsDevice, parent, false),
    ui(new Ui::GpsDevicePane)
{
    ui->setupUi(this);
    setupView(ui->treeView, &mainWindow.gpsModel());
    setWidgets<GpsModel>(ui->queryText, nullptr, ui->showColumns, ui->filterCtrl, ui->queryIsValid,
                         defHideColumns());

    if (contextMenus) // don't do for standalone use.
        setupContextMenus();

    setupActionIcons();
    setupSignals();
}

GpsDevicePane::~GpsDevicePane()
{
    delete ui;
}

void GpsDevicePane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Import_Data, "document-import");
    Icons::defaultIcon(ui->action_Refresh,     "view-refresh");
}

void GpsDevicePane::setupContextMenus()
{

    paneMenu.addActions({ ui->action_Import_Data,
                          ui->action_Refresh });
    paneMenu.addSeparator();

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &GpsDevicePane::customContextMenuRequested, this, &GpsDevicePane::showContextMenu);
}

void GpsDevicePane::setupSignals()
{
    DataColumnPane::setupSignals();

    connect(&mainWindow().gpsModel(), &GpsModel::rowsInserted, this, &GpsDevicePane::resizeOnChange);
}

const GpsDevicePane::ColSet& GpsDevicePane::defHideColumns() const
{
    static const ColSet hide = {
        GpsModel::MountPoint,  // hide these columns by default
        GpsModel::Device,
    };

    return hide;
}

void GpsDevicePane::showContextMenu(const QPoint& pos)
{
    ui->action_Import_Data->setEnabled(hasSelection());

    paneMenu.exec(mapToGlobal(menuPos = pos));
}

void GpsDevicePane::import()
{
    mainWindow().importTracks(mainWindow().gpsModel().importFiles(getSelections()));
}

QTreeView* GpsDevicePane::treeView() const
{
    return ui->treeView;
}

void GpsDevicePane::on_action_Refresh_triggered()
{
    mainWindow().gpsModel().refresh();
}

void GpsDevicePane::resizeOnChange()
{
    resizeToFit(100); // small delay to avoid spam
}

void GpsDevicePane::on_action_Import_Data_triggered()
{
    import();
}

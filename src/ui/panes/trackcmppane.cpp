/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "trackcmppane.h"
#include "ui_trackcmppane.h"

#include <cassert>
#include <numeric>
#include <QInputDialog>
#include <QScrollBar>
#include <QList>
#include <QMap>
#include <QPair>
#include <QToolTip>
#include <QCursor>
#include <QSignalBlocker>
#include <QtCharts/QHorizontalBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>

#include <src/util/util.h>
#include <src/ui/misc/querycompleter.h>
#include <src/util/icons.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/trackmodel.h"

TrackCmpPane::TrackCmpPane(MainWindow& mainWindow, QWidget *parent) :
    ChartBase(mainWindow, PaneClass::CmpChart, parent),
    ui(new Ui::TrackCmpPane),
    graphDataModel(this),
    updateTimer(this),
    currentTimer(this),
    tooltipTimer(this),
    barSeries(new QtCharts::QHorizontalBarSeries()),
    axisY(new QtCharts::QBarCategoryAxis()),
    axisX(new QtCharts::QValueAxis()),
    m_queryRoot(new Query::All()),
    currentRow(-1),
    prevRow(-1),
    plotColumn(-1),
    barWidth(20)
{
    ui->setupUi(this);

    setupActionIcons();
    setPaneFilterBar(ui->filterCtrl);
    setupDataSelector();
    setupTimers();
    setupSignals();
    setupChart();
    setupMenus();
    setupCompleter();
    setupFilterStatusIcons();

    setLockToQuery(false);
}

TrackCmpPane::~TrackCmpPane()
{
    // We don't delete the axes, since the chart owns them (we just track a pointer).

    if (ui != nullptr) {
        ui->graphScroll->takeWidget();  // We own the ChartView, the scroll doesn't.
    }

    delete ui;
}

void TrackCmpPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Show_Axes,     "labplot-axis-vertical");
    Icons::defaultIcon(ui->action_Show_Legend,   "description");
    Icons::defaultIcon(ui->action_Animated,      "motion_path_animations");
    Icons::defaultIcon(ui->action_Set_Bar_Width, "resizerow");
    Icons::defaultIcon(ui->action_Bar_Values,    "format-precision-more");
    Icons::defaultIcon(ui->action_Page_Up,       "arrow-up-double");
    Icons::defaultIcon(ui->action_Page_Down,     "arrow-down-double");
}

void TrackCmpPane::setupTimers()
{
    updateTimer.setSingleShot(true);
    connect(&updateTimer, &QTimer::timeout, this, &TrackCmpPane::updateChart);

    currentTimer.setSingleShot(true);
    connect(&currentTimer, &QTimer::timeout, this, &TrackCmpPane::highlightCurrent);

    // Tooltip reshow: otherwise tooltips disappear over GL widgets no matter what you
    // set the timeout value to.
    connect(&tooltipTimer, &QTimer::timeout, this, &TrackCmpPane::reshowTooltip);
}

void TrackCmpPane::setupChart()
{
    ChartBase::setupChart();

    if (chart == nullptr)
        return;

    chart->addSeries(barSeries);
    chart->legend()->setVisible(false);
    chart->setAnimationDuration(500);

    axisY->setLabelsColor(labelNormal);
    axisY->append("");
    axisY->setVisible(false);

    axisX->setLabelsColor(labelNormal);
    axisX->setGridLinePen(majorGridPen);
    axisX->setMinorGridLinePen(minorGridPen);

    barSeries->setLabelsPosition(QtCharts::QHorizontalBarSeries::LabelsInsideEnd);
    barSeries->setBarWidth(1.0);

    chart->addAxis(axisX, Qt::AlignTop);
    chart->addAxis(axisY, Qt::AlignLeft);
    barSeries->attachAxis(axisX);
    barSeries->attachAxis(axisY);

    ui->graphScroll->setWidget(&chartView);
    setFocusProxy(ui->graphScroll);

    setPlotColumn(0);  // default: must happen after setupDataSelector()
}

void TrackCmpPane::setupMenus()
{
    // We don't add pageup/dn actions, because the widget itself provides keyboard controls for that.
    // Our actions are only for the menu.
    for (auto action : {
         ui->action_Show_Axes })
    {
        action->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        addAction(action);
    }

    paneMenu.addActions({ ui->action_Page_Up,
                          ui->action_Page_Down });
    paneMenu.addSeparator();

    paneMenu.addActions({ ui->action_Animated,
                          ui->action_Bar_Values,
                          ui->action_Show_Axes,
                          ui->action_Set_Bar_Width });
    paneMenu.addSeparator();

    ui->showAxes->setDefaultAction(ui->action_Show_Axes);

    ui->action_Animated->setChecked(true);
    ui->action_Bar_Values->setChecked(false);
    ui->action_Show_Axes->setChecked(true);

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(&chartView, &ChartViewZoom::customContextMenuRequested, this, &TrackCmpPane::showContextMenu);
}

void TrackCmpPane::setupSignals()
{
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &TrackCmpPane::currentTrackChanged);

    connect(&mainWindow().trackModel(), &TrackModel::rowsInserted, this, &TrackCmpPane::processRowsInserted);
    connect(&mainWindow().trackModel(), &TrackModel::rowsRemoved, this, &TrackCmpPane::processRowsRemoved);
    connect(&mainWindow().trackModel(), &TrackModel::dataChanged, this, &TrackCmpPane::processDataChanged);

    connect(barSeries, &QtCharts::QHorizontalBarSeries::hovered, this, &TrackCmpPane::hovered);
    connect(barSeries, &QtCharts::QHorizontalBarSeries::clicked, this, &TrackCmpPane::clicked);

    connect(ui->trackQuery, &QLineEdit::textChanged, this, &TrackCmpPane::queryTextChanged);
}

void TrackCmpPane::setupDataSelector()
{
    ModelMetaData::setupComboBox<TrackModel>(*ui->graphData, graphDataModel, nullptr,
                                             [this](ModelType mt, QStandardItem* item) {
                                                item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
                                                columnMap.append(mt);
                                                return item;
                                             },
                                             [](ModelType mt) { return TrackModel::mdIsChartable(mt); });

    // connect column select behavior
    connect(ui->graphData, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &TrackCmpPane::setPlotColumn);
}

void TrackCmpPane::setupCompleter()
{
    if (ui == nullptr)
        return;

    m_queryCtx.setModel(&mainWindow().trackModel(), cfgData().getFilterCaseSensitivity());

    // the QLineEdit widget takes ownership of the completer: we don't delete or keep track of it.
    QueryCompleter* completer = new QueryCompleter(cfgData(), m_queryCtx, ui->trackQuery);
    ui->trackQuery->setCompleter(completer);
}

void TrackCmpPane::setupFilterStatusIcons()
{
    if (ui != nullptr) {
        const QSize iconSize = ui->filterValid->minimumSize();

        filterEmptyIcon   = cfgData().filterEmptyIcon.pixmap(iconSize);
        filterValidIcon   = cfgData().filterValidIcon.pixmap(iconSize);
        filterInvalidIcon = cfgData().filterInvalidIcon.pixmap(iconSize);
    }

    showFilterStatusIcon();
}

void TrackCmpPane::setAxesShown(bool shown) const
{
    ChartBase::setAxesShown(shown);
    ui->action_Show_Axes->setChecked(shown);

    axisY->setVisible(false);  // always hide Y axis, no matter what.
}

bool TrackCmpPane::axesShown() const
{
    return ui != nullptr && ui->action_Show_Axes->isChecked();
}

void TrackCmpPane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(chartView.mapToGlobal(pos));
}

void TrackCmpPane::setSortDirection(bool checked)
{
    ui->sortDirection->setIcon(checked ? Icons::get("view-sort-ascending") :
                                         Icons::get("view-sort-descending"));
    refreshChart();
}

void TrackCmpPane::setLockToQuery(bool checked)
{
    if (checked)
        connect(&mainWindow(), &MainWindow::trackQueryChanged, this, &TrackCmpPane::externQueryTextChanged, Qt::UniqueConnection);
    else
        disconnect(&mainWindow(), &MainWindow::trackQueryChanged, this, &TrackCmpPane::externQueryTextChanged);

    ui->lockToQuery->setIcon(checked ? Icons::get("object-locked") :
                                       Icons::get("object-unlocked"));

    ui->lockToQuery->setChecked(checked);
}

// This is called from a timer to change the color of the current item
void TrackCmpPane::highlightCurrent()
{
    if (barSeries == nullptr)
        return;

    const TrackModel& model = mainWindow().trackModel();
    const auto barSets = barSeries->barSets();

    // Reset old bar to color it should be
    if (prevRow >= 0 && prevRow < rowToChart.size())
        if (const int chartIdx = rowToChart.at(prevRow); chartIdx >= 0 && chartIdx < barSets.count())
            setBarColorFromModel(model, barSets.at(chartIdx), prevRow);

    // Highlight current
    if (currentRow >= 0 && currentRow < rowToChart.size())
        if (const int chartIdx = rowToChart.at(currentRow); chartIdx >= 0 && chartIdx < barSets.count())
            barSets.at(chartIdx)->setColor(cfgData().trkPtMarkerColor);

    prevRow = currentRow;
}

// Unfortunately, changing a bar color is very slow, likely because it causes
// a complete repaint of the chart, and Qt can't use GL for that as of 5.9.
// As a workaround, we trigger that on a timer to avoid spamming it.  It's not
// as nice, but it means the current track can change far more rapidly.
void TrackCmpPane::currentTrackChanged(const QModelIndex& current)
{
    currentRow = current.isValid() ? current.row() : -1;
    currentTimer.start(250);
}

void TrackCmpPane::showFilterStatusIcon() const
{
    if (ui != nullptr)
        ui->filterValid->setPixmap(ui->trackQuery->text().isEmpty() ? filterEmptyIcon :
                                   m_queryRoot->isValid() ? filterValidIcon : filterInvalidIcon);
}

void TrackCmpPane::queryTextChanged(const QString& query)
{
    emit mainWindow().trackQueryChanged(query);
    setQueryString(query);
}

void TrackCmpPane::externQueryTextChanged(const QString& query)
{
    const QSignalBlocker block(ui->trackQuery);
    ui->trackQuery->setText(query);

    setQueryString(query);
}

void TrackCmpPane::setQueryString(const QString& query)
{
    m_queryRoot.reset(m_queryCtx.parse(query));

    showFilterStatusIcon();
    updateTimer.start(100);
}

void TrackCmpPane::newConfig()
{
    ChartBase::newConfig();

    m_queryCtx.setCaseSensitivity(cfgData().getFilterCaseSensitivity());
    setupFilterStatusIcons();
    updateTimer.start(10);
}

void TrackCmpPane::showAll()
{
    ui->trackQuery->clear();
}

// Tooltip reshow: otherwise tooltips disappear over GL widgets no matter what you
// set the timeout value to.  Ugly hack.
void TrackCmpPane::reshowTooltip()
{
    QToolTip::showText(QCursor::pos(), tooltipText);
}

void TrackCmpPane::hovered(bool status, int /*index*/, QtCharts::QBarSet* barSet)
{
    if (!status) {
        tooltipTimer.stop();
        QToolTip::hideText();
        return;
    }

    const TrackModel& model = mainWindow().trackModel();
    const int row = barSet->property(rowProperty).toInt();

    if (row >= model.rowCount())
        return;

    tooltipText = model.tooltip(model.index(row, TrackModel::Name));
    tooltipPos  = QCursor::pos();

    reshowTooltip();
    tooltipTimer.start(250);
}

// On click, make it the current item in a recent track pane
void TrackCmpPane::clicked(int /*index*/, QtCharts::QBarSet* barSet)
{
    const TrackModel& model = mainWindow().trackModel();
    const int row = barSet->property(rowProperty).toInt();

    if (row >= model.rowCount())
        return;

    TrackPane* trackPane = mainWindow().findPane<TrackPane>();
    if (trackPane == nullptr)
        return;

    trackPane->select(model.index(row, TrackModel::Name),
                      QItemSelectionModel::Current | QItemSelectionModel::Rows);
}

void TrackCmpPane::processRowsInserted(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
    refreshChart();
}

void TrackCmpPane::processRowsRemoved(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
    refreshChart();
}

void TrackCmpPane::processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>&)
{
    if (topLeft.column() > plotColumn || bottomRight.column() < plotColumn)
        return;

    refreshChart();
}

inline void TrackCmpPane::setBarColorFromModel(const TrackModel& model, QtCharts::QBarSet* barSet, int modelRow)
{
    barSet->setColor(model.data(model.index(modelRow, TrackModel::Color), Qt::BackgroundRole).value<QColor>());
}

// There's a QBarSet for each data column we're to display (e.g, distance)
// Each item in the QBarSet corresponds to the column data from a selected TrackItem

void TrackCmpPane::updateChart()
{
    const TrackModel& model = mainWindow().trackModel();

    if (ui == nullptr)
        return;

    currentRow = -1; // reset

    barWidth = std::clamp(barWidth, 4, 256);
    chart->setAnimationOptions(ui->action_Animated->isChecked() ? QtCharts::QChart::SeriesAnimations :
                                                                  QtCharts::QChart::NoAnimation);
    barSeries->setLabelsVisible(ui->action_Bar_Values->isChecked());

    const int modelRowCount = model.rowCount();
    if (modelRowCount == 0 || plotColumn < 0) {
        barSeries->clear();
        return;
    }

    // Add matching rows
    {
        QVector<int> rows;
        rows.reserve(modelRowCount);
        for (int row = 0; row < modelRowCount; ++row)
            if (m_queryRoot->match(model, QModelIndex(), row))
                rows.append(row);

        const bool sortAscending = ui->sortDirection->isChecked();

        // Sort data
        std::sort(rows.begin(), rows.end(), [this, &sortAscending, &model](const int r0, const int r1) {
            const QModelIndex lhs = model.index(r0, plotColumn);
            const QModelIndex rhs = model.index(r1, plotColumn);
            const QVariant lhsData = model.data(lhs, Util::RawDataRole);
            const QVariant rhsData = model.data(rhs, Util::RawDataRole);

            return sortAscending ? (lhsData > rhsData) : (lhsData < rhsData);
        });

        // Early out if nothing has changed since last time
        if (rows == chartToRow)
            return;

        // Swap into the displayedRows vector, and abandon our own.
        rows.swap(chartToRow);
    }

    // Reset selection summary
    selectionSummary.clear(model.rowCount(), chartToRow.size());

    // Clear old graph
    barSeries->clear();
    barSeries->detachAxis(axisX);

    QVector<double> values;
    values.reserve(chartToRow.size());

    double maxVal = -std::numeric_limits<decltype(maxVal)>::max();
    double unitsXMul;

    // Collect data
    for (const auto row : chartToRow) {
        const QModelIndex modelIdx = model.index(row, plotColumn);
        const double value = model.data(modelIdx, Util::RawDataRole).toDouble();
        values.append(value);
        maxVal = std::fmax(maxVal, value);

        selectionSummary.accumulate(model, modelIdx, 1); // for status update
    }
    
    // initial width resize so that the plotArea is sane
    chart->resize(QSizeF(size().width(), chart->size().height()));

    // Calculate normalization values and axis ranges
    {
        const Units& xAxisUnits = TrackModel::mdUnits(plotColumn, cfgData());

        const int padWidth   = QApplication::fontMetrics().size(Qt::TextSingleLine, "M").width();
        const QString labelForWidth = xAxisUnits.chartLabelFormat(1000.0);
        const int labelWidth = QApplication::fontMetrics().size(Qt::TextSingleLine, labelForWidth).width() + padWidth;

        int tickCount = chart->plotArea().width() / std::max(labelWidth, 1);
        maxVal = std::fmax(maxVal, 1.0);

        double niceMin = 0.0, niceMax = maxVal;
        looseNiceNumbers(niceMin, niceMax, tickCount);

        unitsXMul = xAxisUnits.toDouble(1.0); // so we can just multiply later on.

        const double unitsMin = xAxisUnits.toDouble(niceMin);
        const double unitsMax = xAxisUnits.toDouble(niceMax);

        axisX->setRange(unitsMin, unitsMax);
        axisX->setTickCount(tickCount);
        axisX->setMinorTickCount(4);
        axisX->setLabelFormat(xAxisUnits.chartLabelFormat(unitsMax));

        barSeries->setLabelsFormat(QString("@value " + xAxisUnits.suffix(unitsMax)));
    }

    // Scale values to the display unit (there's no built-in display-time scaling)
    assert(chartToRow.size() == values.size());

    // create bar sets
    QList<QtCharts::QBarSet*> barSets;
    barSets.reserve(chartToRow.size());

    // Create rowToIndex map from the indexToRow map
    rowToChart.resize(modelRowCount);
    rowToChart.fill(-1);

    int rowPos = 0;
    for (const auto row : chartToRow) {
        QtCharts::QBarSet* barSet = new QtCharts::QBarSet("");

        barSets.append(barSet);

        barSet->append(values.at(rowPos) * unitsXMul);
        setBarColorFromModel(model, barSet, row);
        barSet->setProperty(rowProperty, row);

        rowToChart[row] = rowPos++;
    }

    barSeries->append(barSets);   // append the list of new barsets we created
    barSeries->attachAxis(axisX); // put X axis back

    // Set size of chart to keep bars the same height no matter how many there are (there's a scroll area)
    const int height = std::max(chartToRow.size() * barWidth, 25) +
                       (chart->size().height() - chart->plotArea().height());

    ui->graphScroll->widget()->resize(ui->graphScroll->contentsRect().width(), height);

    mainWindow().updateStatus(selectionSummary);
}

void TrackCmpPane::resizeEvent(QResizeEvent *event)
{
    ChartBase::resizeEvent(event);

    refreshChart();
}

void TrackCmpPane::focusIn()
{
    mainWindow().updateStatus(selectionSummary);
}

// Force removal of old chart data and refresh of new.
void TrackCmpPane::refreshChart(int delayMs)
{
    chartToRow.clear();
    barSeries->clear();

    if (delayMs > 0)
        updateTimer.start(delayMs);
    else
        updateChart();
}

void TrackCmpPane::setPlotColumn(int index)
{
    plotColumn = (index < columnMap.size()) ? columnMap.at(index) : -1;

    if (ui != nullptr && plotColumn >= 0)
        ui->graphData->setCurrentText(TrackModel::mdName(plotColumn));

    refreshChart(10);
}

void TrackCmpPane::on_lockToQuery_toggled(bool checked)
{
    setLockToQuery(checked);
}

void TrackCmpPane::on_sortDirection_toggled(bool checked)
{
    setSortDirection(checked);
}

void TrackCmpPane::setBarWidth(int val)
{
    if (val == barWidth)
        return;

    barWidth = std::clamp(val, 4, 256);
    refreshChart(10);
}

void TrackCmpPane::setBarValuesShown(bool show)
{
    if (ui == nullptr)
        return;

    ui->action_Bar_Values->setChecked(show);
    refreshChart(10);
}

void TrackCmpPane::on_action_Set_Bar_Width_triggered()
{
    const int val = QInputDialog::getInt(this, tr("Set Bar Width"), tr("Width (px)"), barWidth, 4, 256, 1);

    setBarWidth(val);
}

void TrackCmpPane::on_action_Bar_Values_triggered(bool checked)
{
    setBarValuesShown(checked);
}

void TrackCmpPane::on_action_Page_Up_triggered()
{
    ui->graphScroll->verticalScrollBar()->triggerAction(QScrollBar::SliderPageStepSub);
}

void TrackCmpPane::on_action_Page_Down_triggered()
{
    ui->graphScroll->verticalScrollBar()->triggerAction(QScrollBar::SliderPageStepAdd);
}

void TrackCmpPane::on_action_Show_Axes_triggered(bool shown)
{
    setAxesShown(shown);
}

void TrackCmpPane::save(QSettings& settings) const
{
    ChartBase::save(settings);

    if (ui != nullptr) {
        MemberSave(settings, ui->trackQuery);
        MemberSave(settings, ui->graphData);
        MemberSave(settings, barWidth);

        SL::Save(settings, "lockToTrackQuery", ui->lockToQuery->isChecked());
        SL::Save(settings, "sortAscending", ui->sortDirection->isChecked());
        SL::Save(settings, "animated", ui->action_Animated->isChecked());
        SL::Save(settings, "barValues", ui->action_Bar_Values->isChecked());
    }
}

void TrackCmpPane::load(QSettings& settings)
{
    const QSignalBlocker block(ui->trackQuery);

    ChartBase::load(settings);

    if (ui != nullptr) {
        MemberLoad(settings, ui->trackQuery);
        MemberLoad(settings, ui->graphData);
        MemberLoad(settings, barWidth);

        setPlotColumn(ui->graphData->currentIndex());
        setLockToQuery(SL::Load(settings, "lockToTrackQuery", false));
        setSortDirection(SL::Load(settings, "sortAscending", false));
        setQueryString(ui->trackQuery->text());
        ui->action_Animated->setChecked(SL::Load(settings, "animated", true));
        ui->action_Bar_Values->setChecked(SL::Load(settings, "barValues", true));
    }
}

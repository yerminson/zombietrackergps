/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CATEGORYPANE_H
#define CATEGORYPANE_H

#include <src/ui/widgets/querycompleterdelegate.h>
#include <src/core/query.h>
#include "datacolumnpane.h"

namespace Ui {
class FilterPane;
}

class MainWindow;
class FilterModel;

class FilterPane final : public DataColumnPane
{
    Q_OBJECT

public:
    explicit FilterPane(MainWindow& mainWindow, QWidget *parent = 0);
    ~FilterPane();

    void addFilterInteractive(const QString& query);

private slots:
    void on_FilterPane_toggled(bool checked) { paneToggled(checked); }
    void showContextMenu(const QPoint&);
    void doubleClicked(const QModelIndex&);
    void on_action_Create_New_Filter_triggered();
    void on_action_Edit_Query_triggered();
    void on_action_Activate_triggered();
    void on_action_Set_Icon_triggered();
    void on_action_Edit_Name_triggered();
    void on_action_Unset_Icon_triggered();
    void on_action_Deactivate_triggered();
    void on_action_Update_Filter_triggered();

    void on_action_Duplicate_triggered();

private:
    void setupActionIcons();
    void setupSignals();
    void setupContextMenus();
    void setupDelegates();
    const ColSet& defHideColumns() const override;
    void setFilter();  // set for current selections
    void unsetFilter();
    void newConfig() override;
    void deleteSelection() override;
    bool hasSubtreeSelection(const QModelIndex&) const;
    void buildFilterString(QString& aggregateNames, QString& aggregateQuery, 
                           const QModelIndex& = QModelIndex(), bool firstAtDepth = true);

    PaneBase* getFilterPane() const;

    Ui::FilterPane*        ui;
    QModelIndex            menuIdx;
    Query::Context         m_queryCtx;  // for query language
    QueryCompleterDelegate queryDelegate;
    FilterModel&           model;
};

#endif // CATEGORYPANE_H

/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPPANE_H
#define MAPPANE_H

#include <QPoint>

#include "pane.h"
#include "src/ui/dialogs/mapthemedialog.h"

namespace Ui {
class MapPane;
}

namespace Marble {
class GeoDataLatLonBox;
}

class TrackMap;
class ViewModel;

class MapPane final : public Pane
{
    Q_OBJECT

public:
    explicit MapPane(MainWindow& mainWindow, QWidget *parent = 0);
    ~MapPane();

    void gotoView(const QModelIndex&);
    void zoomTo(const Marble::GeoDataLatLonBox&);
    void addViewPresetInteractive();
    void updateViewPreset(const QModelIndex&);
    void setOfflineMode(bool offline);

    // TODO: ... implement some of these by passing through to TrackPane
//    void showAll() override;
//    void expandAll() override;
//    void collapseAll() override;
//    void selectAll() override;
//    void selectNone() override;
//    void resizeToFit() override;
//    void copySelected() const override;
//    void viewAsTree(bool) override;
    void newConfig() override;

private slots:
    void on_action_Orient_to_North_triggered();
    void on_action_Add_View_Preset_triggered();
    void on_action_Location_to_Clipboard_triggered();
    void on_action_Show_Compass_triggered(bool checked);
    void on_action_Show_Overview_triggered(bool checked);
    void on_action_Show_ScaleBar_triggered(bool checked);
    void on_MapPane_toggled(bool checked) { paneToggled(checked); }
    void on_mapThemeButton_clicked();
    void showContextMenu(const QPoint&);

private:
    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    void setupActionIcons();
    void setupMapWidget();
    void setupSignals();
    void setupContextMenus();
    void refreshMenus();
    void mapZoomWorkaround();

    template <typename T> T viewData(const ViewModel& views, const QModelIndex& idx, int mt);

    Ui::MapPane*     ui;              // map pane UI
    TrackMap*        mapWidget;       // primary map display
    MapThemeDialog*  mapThemeDialog;  // for selecting the map theme
    QPoint           menuPos;         // remember context menu position
};

#endif // MAPPANE_H

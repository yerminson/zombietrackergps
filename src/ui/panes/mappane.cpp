/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <algorithm>
#include <QGuiApplication>
#include <QClipboard>
#include <QMouseEvent>
#include <QInputDialog>
#include <QList>
#include <marble/AbstractFloatItem.h>
#include <marble/GeoDataCoordinates.h>
#include <marble/MarbleWidgetPopupMenu.h>

#include <src/util/roles.h>
#include <src/util/icons.h>
#include "src/core/viewmodel.h"
#include "src/ui/windows/mainwindow.h"
#include "src/ui/widgets/trackmap.h"
#include "mappane.h"
#include "ui_mappane.h"

Q_DECLARE_METATYPE(Marble::MapQuality)

MapPane::MapPane(MainWindow& mainWindow, QWidget *parent) :
    Pane(mainWindow, PaneClass::Map, parent),
    ui(new Ui::MapPane),
    mapWidget(new TrackMap(mainWindow, mainWindow.trackModel())),
    mapThemeDialog(nullptr)
{
    ui->setupUi(this);
    setPaneFilterBar(ui->mapFilterCtrl);

    setupActionIcons();
    setupMapWidget();
    setupSignals();
    setupContextMenus();
    refreshMenus();

    ui->filterMap->setDisabled(true); // TODO: fix when available: https://forum.kde.org/viewtopic.php?t=152494

    setOfflineMode(mainWindow.isOfflineMode()); // initialize to current offline mode
}

MapPane::~MapPane()
{
    delete mapThemeDialog;
    delete ui;
}

void MapPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Show_Overview,         "application-x-marble");
    Icons::defaultIcon(ui->action_Show_ScaleBar,         "tool-measure");
    Icons::defaultIcon(ui->action_Show_Compass,          "compass");
    Icons::defaultIcon(ui->action_Orient_to_North,       "arrow-up");
    Icons::defaultIcon(ui->action_Location_to_Clipboard, "edit-copy");
    Icons::defaultIcon(ui->action_Add_View_Preset,       "mark-location-symbolic");
}

void MapPane::setupMapWidget()
{
    if (mapWidget == nullptr)
        return;

    static const QString defaultThemeId = "earth/openstreetmap/openstreetmap.dgml";

    const Marble::GeoDataCoordinates home(-100.0, 40.0, 0.0, Marble::GeoDataCoordinates::Degree);

    mapWidget->setMapThemeId(defaultThemeId);
    mapWidget->centerOn(home);
    mapWidget->setShowAtmosphere(true);
    mapWidget->setShowBorders(true);
    mapWidget->setShowCompass(false);
    mapWidget->setShowCities(false);
    mapWidget->setShowCrosshairs(false);
    mapWidget->setShowGrid(true);
    mapWidget->setShowOtherPlaces(true);
    mapWidget->setShowOverviewMap(true);
    mapWidget->setShowPlaces(true);
    mapWidget->setShowRelief(true);
    mapWidget->setShowSunShading(false);
    mapWidget->setShowScaleBar(true);
    mapWidget->setShowTerrain(true);
    mapWidget->setProjection(Marble::Spherical);
    mapWidget->setMapQualityForViewContext(Marble::PrintQuality, Marble::Still);
    mapWidget->setAnimationsEnabled(false);

    for (Marble::AbstractFloatItem* floatItem : mapWidget->floatItems()) {
        if (floatItem == nullptr)
            continue;

        // TODO: get size from preferences
        if (floatItem->nameId() == "compass")
            floatItem->setContentSize(QSize(75, 75));

        if (floatItem->nameId() == "license" ||
            floatItem->nameId() == "navigation")
            floatItem->hide();
    }

    // add mapp widget to layout
    ui->verticalLayout->addWidget(mapWidget);
}

void MapPane::setupSignals()
{
    if (mapWidget == nullptr)
        return;

    connect(mapWidget, &TrackMap::mouseMoveGeoPosition, &mainWindow(), &MainWindow::setGeoPositionStatus);
}

// Annoyingly, the map widget has its own menu class and does not accept QMenus.  We add the parent menus
// directly instead of in sub-menus.
void MapPane::setupContextMenus()
{
    const auto addSep = []() {
        QAction* sep = new QAction();
        sep->setSeparator(true);
        return sep;
    };

    Marble::MarbleWidgetPopupMenu* menu = mapWidget->popupMenu();

    menu->addAction(Qt::RightButton, ui->action_Orient_to_North);
    menu->addAction(Qt::RightButton, ui->action_Add_View_Preset);
    menu->addAction(Qt::RightButton, ui->action_Location_to_Clipboard);
    menu->addAction(Qt::RightButton, addSep());

    // Unfortunately, we can only add actions, not menus, to the MarbleWidgetPopupMenu.  This leaves us
    // with a redundant layer of menu "Pane/Pane", but there's little I can find to do about that, so we'll
    // just have to live with it for now.
    QMenu* paneMenu = new QMenu("Pane");
    QAction* paneMenuAction = new QAction("Pane");

    setupPaneContextMenu(*paneMenu);
    paneMenuAction->setMenu(paneMenu);
    menu->addAction(Qt::RightButton, paneMenuAction);

    setContextMenuPolicy(Qt::CustomContextMenu);
}

void MapPane::refreshMenus()
{
    ui->action_Show_Compass->setChecked(mapWidget->showCompass());
    ui->action_Show_Overview->setChecked(mapWidget->showOverviewMap());
    ui->action_Show_ScaleBar->setChecked(mapWidget->showScaleBar());
}

void MapPane::showContextMenu(const QPoint& pos)
{
    if (mapWidget == nullptr)
        return;

    paneMenu.exec(mapToGlobal(menuPos = pos));
}

void MapPane::newConfig()
{
    // TODO: filter case sensitivity
    mapWidget->newConfig();
}

template <typename T>
T MapPane::viewData(const ViewModel& views, const QModelIndex& idx, ModelType mt) {
    return views.data(mt, idx, Util::RawDataRole).value<T>();
}

// This is a workaround for a map widget defect which fails to apply higher render quality sometimes.
void MapPane::mapZoomWorkaround()
{
    mapWidget->zoomOut();
    mapWidget->zoomIn();
}

// Zoom to a view preset
void MapPane::gotoView(const QModelIndex& idx)
{
    if (mapWidget == nullptr || !idx.isValid())
        return;

    const auto& views = mainWindow().viewModel();

    const QString name    = viewData<QString>(views, idx, ViewModel::Name);
    const qreal centerLat = viewData<qreal>(views, idx, ViewModel::CenterLat);
    const qreal centerLon = viewData<qreal>(views, idx, ViewModel::CenterLon);
    const qreal heading   = viewData<qreal>(views, idx, ViewModel::Heading);
    const int   zoom      = viewData<int>(views, idx, ViewModel::Zoom);

    mapWidget->setHeading(heading);
    mapWidget->setZoom(zoom, Marble::Instant);
    mapWidget->centerOn(centerLon, centerLat, false);

    mapZoomWorkaround();
    mainWindow().statusMessage(UiType::Info, tr("Showing: ") + name);
}

void MapPane::zoomTo(const Marble::GeoDataLatLonBox& extent)
{
//    For unknown reasons, this test fails (false positives) on OpenSUSE
//    if (extent.isEmpty())  // unset.
//        return;

    if (extent.isNull()) {  // singularity
        mapWidget->centerOn(extent.center());
    } else {
        mapWidget->centerOn(extent);
    }

    mapZoomWorkaround();
}

void MapPane::save(QSettings& settings) const
{
    if (ui == nullptr || mapWidget == nullptr)
        return;

    Pane::save(settings);

    // TODO: add first class support for hash save/load in settings.h
    // Serialize floating item state for the map (size/colors of overview map, etc)
    settings.beginWriteArray("floatItem"); {
        int itemNo = 0;
        for (const Marble::AbstractFloatItem* floatItem : mapWidget->floatItems()) {
            if (floatItem == nullptr)
                continue;

            settings.setArrayIndex(itemNo++);
            settings.setValue("floatItemNameId", floatItem->nameId());
            settings.setValue("floatItemSettings", floatItem->settings());
            settings.setValue("floatItemVisible", floatItem->visible());
        }
    } settings.endArray();

    SL::Save(settings, "zoom",            mapWidget->zoom());
    SL::Save(settings, "centerLat",       mapWidget->centerLatitude());
    SL::Save(settings, "centerLon",       mapWidget->centerLongitude());
    SL::Save(settings, "mapQualityStill", mapWidget->mapQuality(Marble::Still));
    SL::Save(settings, "mapQualityAnim",  mapWidget->mapQuality(Marble::Animation));
    SL::Save(settings, "mapTheme",        mapWidget->mapThemeId());
    SL::Save(settings, "heading",         mapWidget->heading());
    SL::Save(settings, "showCompass",     mapWidget->showCompass());
    SL::Save(settings, "showOverviewMap", mapWidget->showOverviewMap());
    SL::Save(settings, "showScaleBar",    mapWidget->showScaleBar());

    SL::Save(settings, "filterMap",       ui->filterMap->text());
}

void MapPane::load(QSettings& settings)
{
    if (ui == nullptr || mapWidget == nullptr)
        return;

    Pane::load(settings);

    // TODO: add first class support for hash save/load in settings.h
    // Serialize floating item state for the map (size/colors of overview map, etc)
    const int floatItemCount = settings.beginReadArray("floatItem"); {
        for (int fi = 0; fi < floatItemCount; ++fi) {
            settings.setArrayIndex(fi);

            const QString nameId = settings.value("floatItemNameId").toString();

            // Re-assign it to the proper thing.  This has an obnoxious time complexity, but
            // that doesn't matter for our tiny list size.
            for (Marble::AbstractFloatItem* floatItem : mapWidget->floatItems()) {
                 if (floatItem != nullptr && floatItem->nameId() == nameId) {
                     floatItem->setSettings(settings.value("floatItemSettings").value<QHash<QString, QVariant>>());
                     floatItem->setVisible(settings.value("floatItemVisible").value<bool>());
                 }
            }
        }
    } settings.endArray();

    mapWidget->setZoom(SL::Load<int>(settings, "zoom", 1500));
    mapWidget->setCenterLatitude(SL::Load<qreal>(settings, "centerLat", 40.0));
    mapWidget->setCenterLongitude(SL::Load<qreal>(settings, "centerLon", -100.0));
    mapWidget->setMapQualityForViewContext(SL::Load<Marble::MapQuality>(settings, "mapQualityStill", Marble::HighQuality),
                                           Marble::Still);
    mapWidget->setMapQualityForViewContext(SL::Load<Marble::MapQuality>(settings, "mapQualityAnim", Marble::HighQuality),
                                           Marble::Animation);
    mapWidget->setMapThemeId(SL::Load<QString>(settings, "mapTheme", mapWidget->mapThemeId()));
    mapWidget->setHeading(SL::Load<qreal>(settings, "heading", 0.0));
    mapWidget->setShowCompass(SL::Load<bool>(settings, "showCompass", true));
    mapWidget->setShowOverviewMap(SL::Load<bool>(settings, "showOverviewMap", true));
    mapWidget->setShowScaleBar(SL::Load<bool>(settings, "showScaleBar", true));

    ui->filterMap->setText(SL::Load<QString>(settings, "filterMap"));

    refreshMenus();

    // This is a workaround for a map widget defect which fails to apply higher render quality sometimes.
    mapWidget->zoomOut();
    mapWidget->zoomIn();
}

void MapPane::updateViewPreset(const QModelIndex& idx)
{
    if (mapWidget == nullptr || !idx.isValid())
        return;

    ViewModel& vm = mainWindow().viewModel();

    const QString presetName = vm.data(ViewModel::Name, idx).toString();

    vm.setRow(presetName,
              mapWidget->centerLatitude(),
              mapWidget->centerLongitude(),
              mapWidget->heading(),
              mapWidget->zoom(),
              idx);

    mainWindow().statusMessage(UiType::Success, tr("View preset updated: ") + presetName);
}

void MapPane::setOfflineMode(bool offline)
{
    if (mapWidget != nullptr)
        mapWidget->setOfflineMode(offline);
}

void MapPane::addViewPresetInteractive()
{
    if (mapWidget == nullptr)
        return;

    bool ok;
    const QString newName = QInputDialog::getText(this, tr("View Preset Name"),
                                                  tr("View preset name:"), QLineEdit::Normal,
                                                  "My Map View", &ok);

    if (!ok || newName.isEmpty()) {
        mainWindow().statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    mainWindow().viewModel().appendRow({ newName,
                                         mapWidget->centerLatitude(),
                                         mapWidget->centerLongitude(),
                                         mapWidget->heading(),
                                         mapWidget->zoom() });

    mainWindow().statusMessage(UiType::Success, tr("Added view preset: ") + newName);
}

void MapPane::on_mapThemeButton_clicked()
{
    if (mapThemeDialog == nullptr)
        mapThemeDialog = new MapThemeDialog(this);

    if (mapThemeDialog == nullptr)
        return;

    // If it's already open, close it.
    if (mapThemeDialog->isVisible()) {
        mapThemeDialog->close();
        return;
    }

    // Map dialog location to global coordinates
    const QPoint popLocation = mapToGlobal(QPoint(ui->mapThemeButton->x() - mapThemeDialog->width(),
                                                  ui->mapThemeButton->y() + ui->mapThemeButton->height()));

    mapThemeDialog->setGeometry(QRect(popLocation, mapThemeDialog->size()));
    mapThemeDialog->setWindowFlags(Qt::Popup);
    mapThemeDialog->show();

    connect(mapThemeDialog, &MapThemeDialog::themeSelected, mapWidget, &TrackMap::setMapThemeId,
            Qt::UniqueConnection);
}

void MapPane::on_action_Show_Compass_triggered(bool checked)
{
    if (mapWidget != nullptr)
        mapWidget->setShowCompass(checked);
}

void MapPane::on_action_Show_ScaleBar_triggered(bool checked)
{
    if (mapWidget != nullptr)
        mapWidget->setShowScaleBar(checked);
}

void MapPane::on_action_Show_Overview_triggered(bool checked)
{
    if (mapWidget != nullptr)
        mapWidget->setShowOverviewMap(checked);
}

void MapPane::on_action_Orient_to_North_triggered()
{
    if (mapWidget != nullptr)
        mapWidget->setHeading(0.0);
}

void MapPane::on_action_Location_to_Clipboard_triggered()
{
    if (mapWidget == nullptr)
        return;

    qreal lon, lat;
    mapWidget->geoCoordinates(menuPos.x(), menuPos.y(), lon, lat);

    QGuiApplication::clipboard()->setText(QString("%1 %2")
                                          .arg(lat, 0, 'f', 8)
                                          .arg(lon, 0, 'f', 8));
}

void MapPane::on_action_Add_View_Preset_triggered()
{
    addViewPresetInteractive();
}


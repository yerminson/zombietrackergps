/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QColorDialog>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>

#include <src/util/util.h>

#include "importdialog.h"
#include "ui_importdialog.h"

#include "src/ui/widgets/tagselector.h"

ImportDialog::ImportDialog(const CfgData& cfgData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImportDialog),
    tagSelector(new TagSelector(cfgData)),
    defaultDir(QDir::home().absolutePath())
{
    ui->setupUi(this);
    ui->autoAssignTagsGroupLayout->insertWidget(0, tagSelector);

    Util::SetTBColor(ui->trackColor, QRgb(0x00b155));

    setupSignals();
}

ImportDialog::~ImportDialog()
{
    delete ui;
    // tagSelector is adopted by the UI, so we don't delete it ourselves.
}

void ImportDialog::setupSignals()
{
    connect(ui->overrideTagColors, &QCheckBox::stateChanged, ui->trackColor, &QWidget::setEnabled);

    connect(ui->trackColor, &QToolButton::clicked, this, &ImportDialog::askTrackColor);
}

QStringList ImportDialog::tags() const
{
    return tagSelector->tags();
}

QColor ImportDialog::trackColor() const
{
    return ui->overrideTagColors->isChecked() ? Util::GetTBColor(ui->trackColor) : QColor();
}

bool ImportDialog::deduplicate() const
{
    return ui->dedupliateTracks->isChecked();
}

QStringList ImportDialog::getImportFilenames(const char* filter)
{
    const QStringList trackFiles =
            QFileDialog::getOpenFileNames(parentWidget(),
                                          QObject::tr("Import GPS Track File(s)"), defaultDir,
                                          filter,  nullptr, QFileDialog::ReadOnly);

    if (!trackFiles.isEmpty())
        defaultDir = QFileInfo(trackFiles.front()).path();

    return trackFiles;
}

void ImportDialog::askTrackColor()
{
    Util::SetTBColor(ui->trackColor,
                     QColorDialog::getColor(trackColor(), this, tr("Track Override Color")));
}

void ImportDialog::save(QSettings& settings) const
{
    MemberSave(settings, defaultDir);

    if (ui != nullptr) {
        SL::Save(settings, "trackColor",  trackColor());
        SL::Save(settings, "deduplicate", deduplicate());
    }
}

void ImportDialog::load(QSettings& settings)
{
    MemberLoad(settings, defaultDir);

    if (ui != nullptr) {
        const QColor trackColor = SL::Load(settings, "trackColor", QColor());

        Util::SetTBColor(ui->trackColor, trackColor);
        ui->overrideTagColors->setChecked(trackColor.isValid());
        ui->dedupliateTracks->setChecked(SL::Load(settings, "deduplicate", true));
    }
}

/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "persondialog.h"
#include "ui_persondialog.h"

#include <src/util/roles.h>
#include "src/core/cfgdata.h"

PersonDialog::PersonDialog(CfgData& cfgData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PersonDialog),
    cfgData(cfgData),
    headerView(Qt::Horizontal, this)
{
    ui->setupUi(this);
    setupView();
}

PersonDialog::~PersonDialog()
{
    delete ui;
}

void PersonDialog::setupView()
{
    // We are a read-only view, but setModel doesn't accep ta const pointer,
    // so we have to const-cast.
    ui->personView->setModel(const_cast<PersonModel*>(&cfgData.people));
    ui->personView->setHeader(&headerView);
}

void PersonDialog::setValue(const QString& person)
{
    const QModelIndex idx = cfgData.people.keyIdx(person);
    if (!idx.isValid())
        return;

    ui->personView->selectionModel()->select(idx, QItemSelectionModel::SelectCurrent);
    ui->personView->setCurrentIndex(idx);
    ui->personView->scrollTo(idx);
}

QString PersonDialog::value() const
{
    const QModelIndex idx = ui->personView->selectionModel()->currentIndex();

    if (!idx.isValid())
        return QString();

    return cfgData.people.data(PersonModel::Name, idx, Util::RawDataRole).toString();
}

void PersonDialog::on_personView_doubleClicked(const QModelIndex&)
{
    accept();
}

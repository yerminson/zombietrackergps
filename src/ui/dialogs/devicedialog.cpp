/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QAbstractButton>
#include <QPushButton>
#include <QItemSelectionModel>

#include "src/ui/panes/gpsdevicepane.h"

#include "devicedialog.h"
#include "ui_devicedialog.h"

DeviceDialog::DeviceDialog(MainWindow& mainWindow, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeviceDialog),
    gpsDeviceList(new GpsDevicePane(mainWindow, false)) // disable context menus
{
    ui->setupUi(this);
    setupSignals();

    ui->verticalLayout->insertWidget(0, gpsDeviceList);

    gpsDeviceList->paneToggled(false);  // default to filter bar hidden
    gpsDeviceList->showAllColumns();    // show all columns by default

    processSelectionChanged();
}

DeviceDialog::~DeviceDialog()
{
    delete ui;
    // gpsDeviceList is adopted by the UI, so we don't delete it ourselves.
}

void DeviceDialog::setupSignals()
{
    connect(gpsDeviceList->treeView()->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &DeviceDialog::processSelectionChanged);
}

void DeviceDialog::processSelectionChanged()
{
    ui->importButton->setEnabled(gpsDeviceList->treeView()->selectionModel()->hasSelection());
}

void DeviceDialog::on_importButton_clicked()
{
    hide();
    gpsDeviceList->import();
}

/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tagrenamedialog.h"
#include "ui_tagrenamedialog.h"

TagRenameDialog::TagRenameDialog(const QSet<QString>& renames, const QSet<QString>& deletions, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TagRenameDialog)
{
    ui->setupUi(this);

    addRenames(renames);
    addRemovals(deletions);
}

TagRenameDialog::~TagRenameDialog()
{
    delete ui;
}

bool TagRenameDialog::tagRenameUpdate() const
{
    return ui->tagRenameUpdate->isChecked();
}

bool TagRenameDialog::tagRenameLeave() const
{
    return ui->tagRenameLeave->isChecked();
}

bool TagRenameDialog::tagRenameRemove() const
{
    return ui->tagRenameRemove->isChecked();
}

bool TagRenameDialog::tagRemoveRemove() const
{
    return ui->tagRemoveRemove->isChecked();
}

bool TagRenameDialog::tagRemoveLeave() const
{
    return ui->tagRemoveLeave->isChecked();
}

void TagRenameDialog::addRenames(const QSet<QString>& tags)
{
    for (int i = 0; i < ui->renameOpts->layout()->count(); ++i)
        ui->renameOpts->layout()->itemAt(i)->widget()->setEnabled(!tags.isEmpty());
    ui->renamedTags->setEnabled(!tags.isEmpty());

    for (const auto& tag : tags)
        ui->renamedTags->addItem(tag);
}

void TagRenameDialog::addRemovals(const QSet<QString>& tags)
{
    for (int i = 0; i < ui->removeOpts->layout()->count(); ++i)
        ui->removeOpts->layout()->itemAt(i)->widget()->setEnabled(!tags.isEmpty());
    ui->removedTags->setEnabled(!tags.isEmpty());

    for (const auto& tag : tags)
        ui->removedTags->addItem(tag);
}

/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tagselectordialog.h"
#include "ui_tagselectordialog.h"

#include "src/ui/widgets/tagselector.h"

TagSelectorDialog::TagSelectorDialog(const CfgData& cfgData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TagSelectorDialog),
    tagSelector(new TagSelector(cfgData))
{
    ui->setupUi(this);
    ui->verticalLayout->insertWidget(0, tagSelector);
}

TagSelectorDialog::~TagSelectorDialog()
{
    delete ui;
    // tagSelector is adopted by the UI, so we don't delete it ourselves.
}


QStringList TagSelectorDialog::tags() const
{
    return tagSelector->tags();
}

void TagSelectorDialog::setTags(const QStringList& tags)
{
    tagSelector->setTags(tags);
}

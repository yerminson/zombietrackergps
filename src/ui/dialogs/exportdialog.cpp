/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDir>
#include <QFileDialog>
#include <QFileInfo>

#include "exportdialog.h"
#include "ui_exportdialog.h"

#include <src/geo-io/geoiogpx.h> // just for the default name

ExportDialog::ExportDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExportDialog)
{
    ui->setupUi(this);

    ui->exportFormat->addItems(GeoSave::formatNames());
    ui->exportFormat->setCurrentText(GeoLoadGpx::name);

    defaultDir = QDir::home().absolutePath();
}

ExportDialog::~ExportDialog()
{
    delete ui;
}

GeoFormat ExportDialog::exportFormat() const
{
    return GeoFormat(ui->exportFormat->currentIndex() + 1);  // +1 skips native, which we don't offer.
}

bool ExportDialog::writeFormatted() const
{
    return ui->writeFormatted->isChecked();
}

int ExportDialog::indentLevel() const
{
    return ui->indentLevel->value();
}

bool ExportDialog::indentSpaces() const
{
    return ui->writeSpaces->isChecked();
}

bool ExportDialog::exportAll() const
{
    return ui->writeAllTracks->isChecked();
}

QString ExportDialog::getExportFileName(const char* filter)
{
    const QString trackFile =
            QFileDialog::getSaveFileName(parentWidget(),
                                         QObject::tr("Export GPS Track File"), defaultDir,
                                         filter);

    if (!trackFile.isEmpty())
        defaultDir = QFileInfo(trackFile).path();

    return trackFile;
}

void ExportDialog::save(QSettings& settings) const
{
    MemberSave(settings, defaultDir);

    if (ui != nullptr) {
        SL::Save(settings, "exportFormat",   ui->exportFormat->currentText());
        SL::Save(settings, "writeFormatted", writeFormatted());
        SL::Save(settings, "indentLevel",    indentLevel());
        SL::Save(settings, "indentSpaces",   indentSpaces());
        SL::Save(settings, "exportAll",      exportAll());
    }
}

void ExportDialog::load(QSettings& settings)
{
    MemberLoad(settings, defaultDir);

    if (ui != nullptr) {
        ui->exportFormat->setCurrentText(SL::Load(settings, "exportFormat", GeoLoadGpx::name));
        ui->writeFormatted->setChecked(SL::Load(settings, "writeFormatted", false));
        ui->indentLevel->setValue(SL::Load(settings, "indentLevel", 2));
        ui->writeSpaces->setChecked(SL::Load(settings, "indentSpaces", true));
        ui->writeTabs->setChecked(!SL::Load(settings, "indentSpaces", true));
        ui->writeAllTracks->setChecked(SL::Load(settings, "exportAll", true));
    }
}

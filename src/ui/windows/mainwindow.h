/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <array>

#include <QElapsedTimer>
#include <QTimer>
#include <QList>
#include <QLabel>
#include <QDir>
#include <QProgressBar>
#include <QSessionManager>

#include <src/ui/windows/mainwindowbase.h>
#include "src/core/selectionsummary.h"
#include "src/core/filtermodel.h"
#include "src/core/viewmodel.h"
#include "src/core/trackmodel.h"
#include "src/dev-io/gpsmodel.h"
#include "src/ui/panes/pane.h"
#include "src/ui/windows/appconfig.h"
#include "src/ui/windows/aboutdialog.h"
#include "src/ui/windows/docdialog.h"
#include "src/ui/dialogs/exportdialog.h"
#include "src/ui/dialogs/importdialog.h"
#include "src/ui/dialogs/areadialog.h"
#include "src/ui/dialogs/persondialog.h"
#include "src/ui/dialogs/devicedialog.h"

enum class UiType;
class QItemSelectionModel;

namespace Ui {
class MainWindow;
}

enum class IconType {
    Points,  // for drawing map line points
    Tags,    // icons for track tag types
    _Count,
};

// Actions presented in main window, e.g, by toolbar
enum class MainAction {
    ZoomToSelection,
    DeleteSelection,
    SelectPerson,
};

class MainWindow final : public MainWindowBase
{
    Q_OBJECT

public:
    explicit MainWindow(const char* initialSettingsFile = "", QWidget *parent = 0);
    ~MainWindow();

    const CfgData& cfgData() const override { return appConfig.getCfgData(); }
    const CfgData& constCfgData() { return appConfig.getCfgData(); }

    QAction*  getPaneAction(PaneAction cc) const override;
    QAction*  getMainAction(MainAction aa) const;

    FilterModel&   filterModel()         { return m_filterModel; }
    ViewModel&     viewModel()           { return m_viewModel; }
    TrackModel&    trackModel()          { return m_trackModel; }
    const TrackModel& trackModel() const { return m_trackModel; }
    GpsModel&      gpsModel()            { return m_gpsModel; }

    QWidget*  paneFactory(int paneClass = -1) const override;
    template <class T = PaneBase> T* paneFactory(PaneClass paneClass) const;
    Pane::Container* containerFactory() const override;

    class ProgressHandler {
    public:
        ProgressHandler(MainWindow& m, int total) : m(m) { if (nest++ == 0) m.initProgress(total); }
        ~ProgressHandler() { if (--nest == 0) m.finishProgress(); }
    private:
        MainWindow& m;
        static int nest;
    };

    void updateStatus(const SelectionSummary& selectionSummary);

    void updateProgress(int done, bool add = false);
    void failedProgress(int failed);
    void finishProgress();
    void initProgress(int total);
    void showBoxSelectionDialog(const Marble::GeoDataLatLonBox& region);

    void importTracks();
    void importTracks(const QStringList&);

    bool isOfflineMode() const;

    static MainWindow* mainWindowStatic;

private:
    // Models we save and load
    enum class Model {
        _First,
        Track = _First,
        View,
        Filter,
        _Count,
    };

    // Status bar items
    enum class Stat {
        Offline = 0,
        Total,
        Visible,
        Selected,
        Length,
        Duration,
        Ascent,
        Descent,
        GeoLocation,
        _Count,
    };

public slots:
    void newConfig() override;
    void viewAsTree(bool);
    void statusChanged(const QString&) const; // reset status line color
    void updateStatus();
    void selectionChanged(const QItemSelectionModel*,
                          const QItemSelection& selected, const QItemSelection& deselected) override;
    void currentChanged(const QModelIndex& current) override;
    void commitData(QSessionManager& manager);
    void setGeoPositionStatus(const QString&);

signals:
    void currentTrackChanged(const QModelIndex& current);
    void visibleTracksChanged();
    void currentTrackPointChanged(const QModelIndex& current);
    void selectedPointsChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&);
    void selectedTracksChanged(const QItemSelectionModel*, const QItemSelection&, const QItemSelection&);
    void trackQueryChanged(const QString&);

private slots:
    void newFocus(QObject*) override;
    void postLoadHook() override;
    void dataChange(Model, bool dirty); // for modification flag and saves
    void saveModels();  // save all binary models
    void loadModels();  // load all binary models

    // TODO: ...
    //    void commitData(QSessionManager& manager);
    //    void egg();

    void on_action_Copy_Selected_triggered();
    void on_action_Enlarge_Font_triggered();
    void on_action_Reset_Ui_triggered();
    void on_action_Import_Track_triggered();
    void on_action_Export_Track_triggered();
    void on_action_Open_Settings_triggered();
    void on_action_Pane_Balance_Siblings_triggered();
    void on_action_Pane_Close_triggered();
    void on_action_Pane_Group_Left_triggered();
    void on_action_Pane_Group_Right_triggered();
    void on_action_Pane_Left_triggered();
    void on_action_Pane_Right_triggered();
    void on_action_Pane_Split_Horizontal_triggered();
    void on_action_Pane_Split_Vertical_triggered();
    void on_action_Quit_without_Save_triggered();
    void on_action_Reset_Font_triggered();
    void on_action_Revert_triggered();
    void on_action_Save_Settings_As_triggered();
    void on_action_Save_Settings_triggered();
    void on_action_Show_Filters_triggered(bool checked);
    void on_action_Shrink_Font_triggered();
    void on_action_Size_Cols_triggered();
    void on_action_View_Collapse_All_triggered();
    void on_action_View_Expand_All_triggered();
    void on_action_View_Select_All_triggered();
    void on_action_View_Select_None_triggered();
    void on_action_View_Show_All_triggered();
    void on_action_Zoom_To_Selection_triggered();
    void on_action_Delete_Selection_triggered();
    void on_action_Select_Person_triggered();
    void on_action_Import_from_Device_triggered();
    void on_action_New_Window_triggered();
    void on_action_Offline_Mode_triggered(bool checked);
    void on_action_Donate_triggered();

private:
    CfgData& cfgData() override { return appConfig.getCfgData(); }

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    void setupSession();
    void setupDefaultPanels();
    void setupDefaultViewPresets();
    void setupDefaultFilters();
    void setupIcons();
    void setupMenus();
    void setupStatus();
    void setupSignals();
    void setupAutosave();
    void setupDefaults();
    void setupActionIcons();
    void updateActions();
    void firstRunHook() override;
    void triggerSaveIfDirty();
    using MainWindowBase::recentSessionsChanged;
    void recentSessionsChanged() override;
    void progressTimerUpdate(); // see comment in C++
    void postImport(); // post import cleanup: resize columns, etc.
    void setOfflineMode(bool offline);

    QString modelDataFile(Model) const;
    TreeModel& getModel(Model);
    const TreeModel& getModel(Model) const;
    bool isModelDirty(Model mtype) const { return m_modelDirty.at(int(mtype)); }
    void setModelsDirty(bool dirty) { m_modelDirty.fill(dirty); }

    // set status bar data
    void setStat(Stat s, const QString&, const QByteArray& fontStyle, const QColor& color);
    void setStat(Stat s, const QString&, const QByteArray& fontStyle = "normal");

    Ui::MainWindow*        ui;

    // Models
    FilterModel            m_filterModel;
    ViewModel              m_viewModel;
    TrackModel             m_trackModel;
    GpsModel               m_gpsModel;

    // Config
    AppConfig              appConfig;
    QString                m_person; // for power estimations

    // Dialogs
    AboutDialog            aboutDialog;
    DocDialog              docDialog;
    ExportDialog           exportOpts;
    ImportDialog           importOpts;
    AreaDialog             areaDialog;
    PersonDialog           personDialog;
    DeviceDialog           deviceDialog;

    QElapsedTimer          progressTimer;
    QTimer                 autosaveTimer;

    std::array<QLabel, int(Stat::_Count)>        m_statusText;   // status bar text
    mutable std::array<bool, int(Model::_Count)> m_modelDirty;   // per-model dirty bit
    mutable QString                              m_modelVisited; // last model location visited
    bool                                         m_modelLoading; // pending model load

    int                    m_progressValue;                      // see progressTimerUpdate comment
    int                    m_progressMax;                        // ...

    // binary save magic values
    static const constexpr quint32 NativeVersion = 0x1000;
    static const constexpr quint32 NativeMagic   = 0xd9baf758;
};

#endif // MAINWINDOW_H

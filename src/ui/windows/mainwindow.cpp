/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QMessageBox>
#include <QIcon>
#include <QAction>
#include <QMenu>
#include <QWhatsThis>
#include <QItemSelectionModel>
#include <QSignalBlocker>
#include <QStandardPaths>

#include <algorithm>
#include <cassert>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "src/version.h"
#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/versionedstream.h>
#include <src/ui/widgets/tabwidget.h>
#include "src/ui/panes/trackpane.h"
#include "src/ui/panes/trackcmppane.h"
#include "src/geo-io/geoio.h"
#include "src/core/pointmodel.h"
#include "src/core/personmodel.h"

int MainWindow::ProgressHandler::nest = 0;
MainWindow* MainWindow::mainWindowStatic = nullptr;

MainWindow::MainWindow(const char* initialSettingsFile, QWidget *parent) :
    MainWindowBase(::apptitle, initialSettingsFile, parent),
    ui(new Ui::MainWindow),
    m_filterModel(cfgData(), this),
    m_viewModel(cfgData(), this),
    m_trackModel(cfgData(), this),
    m_gpsModel(cfgData(), this),
    appConfig(this),
    aboutDialog(this),
    docDialog(this),
    exportOpts(this),
    importOpts(cfgData(), this),
    areaDialog(*this, this),
    personDialog(cfgData(), this),
    deviceDialog(*this, this),
    autosaveTimer(this),
    m_modelDirty(),
    m_modelLoading(false),
    m_progressValue(1),
    m_progressMax(1)
{
    ui->setupUi(this);

    mainWindowStatic = this;   // avoid passing singleton through N layers
    ui->centralWidget->layout()->addWidget(new TabWidget(*this));

    resize(1920, 1280);        // default window size

    setupSession();            // session management
    setupIcons();              // setup window/taskbar icons
    setupMenus();              // menu slot connections
    setupStatus();             // create status bar widgets
    setupSignals();            // connect signals/slots
    setupAutosave();           // initialize timers
    setupAppConfig();          // application name, etc
    setupActionIcons();        // default icons if there's no theme icon

    sessionRestore();          // restore any prior session
    setupDefaultPanels();      // setup default panel config (after loading session)
    recentSessionsChanged();   // virtual method can't be invoked from parent constructor
}

MainWindow::~MainWindow()
{
    cleanup(); // things to do before removing the GUI.

    delete ui;
    ui = nullptr;
}

void MainWindow::setupSession()
{
    QGuiApplication::setFallbackSessionManagementEnabled(false);
    connect(qApp, &QApplication::commitDataRequest, this, &MainWindow::commitData);
}

void MainWindow::setupIcons()
{
    QApplication::setWindowIcon(QIcon(":art/logos/projects/zombietrackergps.svg"));
    setWindowIcon(QIcon(":art/logos/projects/zombietrackergps.svg"));
}

QWidget* MainWindow::paneFactory(int paneClass) const
{
    if (PaneClass(paneClass) == PaneClass::Group)
        return containerFactory();

    if (paneClass < int(PaneClass::_First) || paneClass >= int(PaneClass::_Count))
        paneClass = int(PaneClass::Empty);

    return Pane::factory<QWidget>(PaneClass(paneClass), const_cast<MainWindow&>(*this));
}

template <class T> T* MainWindow::paneFactory(PaneClass paneClass) const
{
    return dynamic_cast<T*>(paneFactory(int(paneClass)));
}

// Default panel config
void MainWindow::setupDefaultPanels()
{
    TabWidget* tabs = mainWindowTabs();

    const QSignalBlocker bts(tabs);

    if (tabs->count() > 1)  // skip if the session loaded panels already.
        return;

    auto* mapPane    = paneFactory(PaneClass::Map);
    auto* filterPane = paneFactory(PaneClass::Filter);
    auto* viewPane   = paneFactory(PaneClass::View);
    auto* pointPane  = paneFactory(PaneClass::Points);
    auto* trackPane  = paneFactory(PaneClass::Track);
    auto* lineChart  = paneFactory(PaneClass::LineChart);
    auto* cmpChart   = paneFactory<TrackCmpPane>(PaneClass::CmpChart);
    auto* vGroupL    = containerFactory();
    auto* vGroupM    = containerFactory();
    auto* hGroupB    = containerFactory();
    auto* hGroupC    = containerFactory();

    // Set up some init-time defaults
    cmpChart->setBarWidth(12);
    cmpChart->setBarValuesShown(false);

    // Toggle UI bar off for some of these (different from the new-pane default)
    for (PaneBase* pane : QList<PaneBase*>({ viewPane, filterPane, pointPane, lineChart, cmpChart }))
        pane->paneToggled(false);

    vGroupL->setOrientation(Qt::Vertical);
    vGroupM->setOrientation(Qt::Vertical);
    hGroupB->setOrientation(Qt::Horizontal);
    hGroupC->setOrientation(Qt::Horizontal);

    // VGroup: LHS vertical
    addPane(filterPane, vGroupL);
    addPane(viewPane,   vGroupL);
    addPane(pointPane,  vGroupL);

    // Hgroup: LHS vertical group + map
    addPane(vGroupL,    hGroupC, 0);
    addPane(mapPane,    hGroupC);

    // HGroup: Bottom horizontal
    addPane(trackPane,  hGroupB);
    addPane(cmpChart,   hGroupB);
    addPane(lineChart,  hGroupB);

    // VGroup: main tab VGroup
    addPane(hGroupC, vGroupM, 0);
    addPane(hGroupB, vGroupM, 1);

    tabs->addTab("Main", vGroupM);

    mapPane->setFocus();

    tabs->setCurrentIndex(0);

    resizeColumnsAllPanes();

    hGroupC->setSizes({ 200, 800 });
    hGroupC->setStretchFactor(0, 20);
    hGroupC->setStretchFactor(1, 80);

    hGroupB->setSizes({ 600, 200, 300 });
    hGroupB->setStretchFactor(0, 60);
    hGroupB->setStretchFactor(1, 20);
    hGroupB->setStretchFactor(2, 30);

    vGroupM->setSizes({ 750, 250 });
    vGroupM->setStretchFactor(0, 75);
    vGroupM->setStretchFactor(1, 25);
}

// Example default queries, intended to be updated/replaced/etc by user.
void MainWindow::setupDefaultFilters()
{
    FilterModel& model = filterModel();

    if (model.rowCount() > 0)
        return;

    model.appendRow({ "Bike",  "Tags =~ Commute|Cross|Mountain|Road|TT" });
    model.appendRow({ "Foot",  "Tags =~ Hike|Run" });
    model.appendRow({ "Water", "Tags =~ Canoe|Sail|Swim" });

    const QModelIndex bike  = model.index(0, 0);
    const QModelIndex foot  = model.index(1, 0);
    const QModelIndex water = model.index(2, 0);

    model.appendRow({ "Big Climbs", "Ascent > 500 m" }, bike);
    const QModelIndex climbs = model.index(0, 0, bike);

    model.setSiblingIcon(FilterModel::Icon, bike,   ":art/tags/Transport/Road.svg");
    model.setSiblingIcon(FilterModel::Icon, foot,   ":art/tags/Transport/Run.svg");
    model.setSiblingIcon(FilterModel::Icon, water,  ":art/tags/Transport/Sail.svg");
    model.setSiblingIcon(FilterModel::Icon, climbs, ":art/tags/Category/Ascend.svg");
}

// Example view presets, intended to be updated/replaced/etc by user.
void MainWindow::setupDefaultViewPresets()
{
    ViewModel& model = viewModel();

    if (model.rowCount() > 0)
        return;

    model.appendRow({ "Canada", 57.98525257396584, -93.8615056166874, 0.0, 1525 });
    model.appendRow({ "France", 46.69553533747557, 2.486498135897202, 0.0, 1660 });
    model.appendRow({ "USA", 40.212622574023236, -98.23197951780352, 0.0, 1525 });

    const QModelIndex canada = model.index(0, 0);
    const QModelIndex france = model.index(1, 0);
    const QModelIndex usa    = model.index(2, 0);

    model.appendRow({ "Minnesota", 46.15787512003232, -92.86976379630738, 0.0, 1810 }, usa);
    model.appendRow({ "Washington", 46.92173418716036, -122.58105221389884, 0.0, 1950 }, usa);

    const QModelIndex MN = model.index(0, 0, usa);
    const QModelIndex WA = model.index(1, 0, usa);

    model.setSiblingIcon(ViewModel::Icon, canada, ":art/tags/Flags/Countries/Canada.jpg");
    model.setSiblingIcon(ViewModel::Icon, france, ":art/tags/Flags/Countries/France.jpg");
    model.setSiblingIcon(ViewModel::Icon, usa,    ":art/tags/Flags/Countries/United_States.jpg");

    model.setSiblingIcon(ViewModel::Icon, MN,     ":art/tags/Flags/Regions/United_States/Minnesota.jpg");
    model.setSiblingIcon(ViewModel::Icon, WA,     ":art/tags/Flags/Regions/United_States/Washington.jpg");
}

void MainWindow::setupStatus()
{
    for (auto& t : m_statusText) {
       statusBar()->addWidget(&t);
       if (&t != &m_statusText.back())
           statusBar()->addWidget(new QLabel("|"));
    }

    // Add progress bar
    progressBar.setOrientation(Qt::Horizontal);
    progressBar.setMaximumWidth(600);
    finishProgress();
    statusBar()->addPermanentWidget(&progressBar);

    connect(statusBar(), &QStatusBar::messageChanged, this, &MainWindow::statusChanged);
}

// Tedious: fix actions which didn't find icons in any default theme.  See comment in Icons get() method
// for why this appears necessary.
void MainWindow::setupActionIcons()
{
    Icons::defaultIcon(ui->action_About_Qt,              "system-help");
    Icons::defaultIcon(ui->action_About_ZT,              "help-about");
    Icons::defaultIcon(ui->action_Configure,             "configure");
    Icons::defaultIcon(ui->action_Copy_Selected,         "edit-copy");
    Icons::defaultIcon(ui->action_Delete_Selection,      "delete");
    Icons::defaultIcon(ui->action_Donate,                "help-donate");
    Icons::defaultIcon(ui->action_Enlarge_Font,          "format-font-size-more");
    Icons::defaultIcon(ui->action_Export_Track,          "document-save");
    Icons::defaultIcon(ui->action_Import_Track,          "document-open");
    Icons::defaultIcon(ui->action_Import_from_Device,    "kstars_satellites");
    Icons::defaultIcon(ui->action_New_Window,            "window-new");
    Icons::defaultIcon(ui->action_Offline_Mode,          "offline");
    Icons::defaultIcon(ui->action_Open_Settings,         "document-open");
    Icons::defaultIcon(ui->action_Pane_Balance_Siblings, "object-columns");
    Icons::defaultIcon(ui->action_Pane_Close,            "tab-close");
    Icons::defaultIcon(ui->action_Pane_Group_Left,       "go-previous");
    Icons::defaultIcon(ui->action_Pane_Group_Right,      "go-next");
    Icons::defaultIcon(ui->action_Pane_Left,             "go-previous");
    Icons::defaultIcon(ui->action_Pane_Right,            "go-next");
    Icons::defaultIcon(ui->action_Pane_Split_Horizontal, "view-split-left-right");
    Icons::defaultIcon(ui->action_Pane_Split_Vertical,   "view-split-top-bottom");
    Icons::defaultIcon(ui->action_Quit,                  "application-exit");
    Icons::defaultIcon(ui->action_Quit_without_Save,     "application-exit");
    Icons::defaultIcon(ui->action_Reset_Font,            "application-x-font-ttf");
    Icons::defaultIcon(ui->action_Reset_Ui,              "document-new");
    Icons::defaultIcon(ui->action_Revert,                "document-revert");
    Icons::defaultIcon(ui->action_Save_Settings,         "document-save");
    Icons::defaultIcon(ui->action_Save_Settings_As,      "document-save-as");
    Icons::defaultIcon(ui->action_Select_Person,         "system-switch-user");
    Icons::defaultIcon(ui->action_Show_Filters,          "view-filter");
    Icons::defaultIcon(ui->action_Show_Statusbar,        "kt-show-statusbar");
    Icons::defaultIcon(ui->action_Show_Toolbar,          "configure-toolbars");
    Icons::defaultIcon(ui->action_Show_Tutorial,         "documentation");
    Icons::defaultIcon(ui->action_Shrink_Font,           "format-font-size-less");
    Icons::defaultIcon(ui->action_Size_Cols,             "zoom-fit-best");
    Icons::defaultIcon(ui->action_View_AsTree,           "view-list-tree");
    Icons::defaultIcon(ui->action_View_Collapse_All,     "format-indent-less");
    Icons::defaultIcon(ui->action_View_Expand_All,       "format-indent-more");
    Icons::defaultIcon(ui->action_View_Select_All,       "edit-select-all");
    Icons::defaultIcon(ui->action_View_Select_None,      "edit-select-none");
    Icons::defaultIcon(ui->action_View_Show_All,         "view-restore");
    Icons::defaultIcon(ui->action_What_is,               "help-whatsthis");
    Icons::defaultIcon(ui->action_Zoom_To_Selection,     "zoom-fit-selection");

}

void MainWindow::setupMenus()
{
    const auto newAction = [&](QList<QAction*>& actions, PaneClass pc) {
        if (QAction* action = new QAction(Pane::icon(pc), Pane::name(pc), this)) {
            action->setToolTip(Pane::tooltip(pc));
            action->setWhatsThis(Pane::tooltip(pc));
            action->setStatusTip(Pane::tooltip(pc));
            actions.push_back(action);
        }
    };

    // Create actions for adding all the kinds of panes we have.
    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc)) {
        newAction(paneAddActions, pc);
        newAction(paneGrpActions, pc);
        newAction(paneRepActions, pc);
        newAction(paneWinActions, pc);
    }

    ui->menuPane->addSeparator();
    ui->menuPane->addActions({ getPaneAction(PaneAction::PaneBalanceTab),
                               getPaneAction(PaneAction::PaneAddTab),
                               getPaneAction(PaneAction::PaneRenameTab),
                               getPaneAction(PaneAction::PaneCloseTab) });

    ui->menu_Add_Pane->addActions(paneClassAddActions());
    connect(ui->menu_Add_Pane, &QMenu::triggered, this, &MainWindow::addPaneAction);

    ui->menu_Add_Group->addActions(paneClassGrpActions());
    connect(ui->menu_Add_Group, &QMenu::triggered, this, &MainWindow::addGroupAction);

    ui->menu_Replace_Pane->addActions(paneClassRepActions());
    connect(ui->menu_Replace_Pane, &QMenu::triggered, this, &MainWindow::replacePaneAction);

    ui->menu_Pane_in_Window->addActions(paneClassWinActions());
    connect(ui->menu_Pane_in_Window, &QMenu::triggered, this, &MainWindow::paneInWindowAction);

    // menu connections
    connect(ui->action_About_Qt, &QAction::triggered, QCoreApplication::instance(), &QApplication::aboutQt);
    connect(ui->action_Configure, &QAction::triggered, &appConfig, &AppConfig::show);

    connect(ui->action_View_AsTree, &QAction::triggered, this, &MainWindow::viewAsTree);
    connect(ui->action_About_ZT, &QAction::triggered, &aboutDialog, &AboutDialog::show);
    connect(ui->action_Show_Tutorial, &QAction::triggered, &docDialog, &DocDialog::show);
    connect(ui->action_What_is, &QAction::triggered, this, &QWhatsThis::enterWhatsThisMode);

    // Recent sessions menu
    connect(ui->menu_Recent_Sessions, &QMenu::triggered, this, &MainWindow::loadRecentSession);
}

void MainWindow::recentSessionsChanged()
{
    if (ui == nullptr)
        return;

    return recentSessionsChanged(ui->menu_Recent_Sessions);
}

void MainWindow::firstRunHook()
{
    MainWindowBase::firstRunHook();

    // Ask user about loading sample data on first run.
    // TODO: for some reason, supplying the main window as the QMessageBox parent puts the message box on
    // the wrong screen in multi-monitor setups.  Unclear why.  Use nullptr instead, which centers it on
    // the primary display.
    const QMessageBox::StandardButton result =
            QMessageBox::question(nullptr, tr("Import Sample Data?"),
                                  tr("This is the first time ") + QApplication::applicationDisplayName() +
                                  tr(" has been run with this profile.  You can import some sample data to "
                                     "experiment with, or start with a clean slate.  Would you like to import "
                                     "the sample data?"),
                                  QMessageBox::Yes | QMessageBox::No,
                                  QMessageBox::Yes);

    if (result == QMessageBox::Yes) {
        setupDefaultFilters();     // default track filters
        setupDefaultViewPresets(); // default view presets

        const QString sampleData = QStandardPaths::locate(QStandardPaths::AppDataLocation, "sample/mystic_basin_trail.gpx");
        bool loadOK = false;
        if (!sampleData.isEmpty()) {
            GeoLoad loader(*this, trackModel(), {"Road", "Recreation"});
            loadOK = loader.requestDeduplicate(true).load({sampleData}) && (trackModel().rowCount() > 0);

            if (loadOK) {
                if (TrackPane* trackPane = findPane<TrackPane>(); trackPane != nullptr) {
                    trackPane->selectAll();
                    trackPane->zoomToSelection();
                    trackPane->select(trackModel().index(0, 0),
                                      QItemSelectionModel::Clear | QItemSelectionModel::Current | QItemSelectionModel::Rows);
                    trackPane->setFocus();
                }

                runOnPanels([](PaneBase* pane) { pane->expandAll(); });
                postImport();
            }
        }

        if (!loadOK)
            error(tr("Unable to import sample data.  Please verify the program installation."), tr("Import Sample Data"));
    }

    docDialog.show();  // show intro dialog
}

void MainWindow::commitData(QSessionManager& /*manager*/)
{
    sessionSave();
}

void MainWindow::updateActions()
{
    PaneBase* focused = focusedPane();
    const bool hasFocused      = (focused != nullptr);
    const bool hasSelection    = hasFocused && focused->hasSelection();
    const bool hasItems        = hasFocused && focused->hasItems();
    const bool hasSettingsFile = MainWindowBase::hasSettingsFile();

    ui->action_Zoom_To_Selection->setEnabled(hasSelection);
    ui->action_Delete_Selection->setEnabled(hasSelection);
    ui->action_Copy_Selected->setEnabled(hasSelection);
    ui->action_View_Expand_All->setEnabled(hasItems);
    ui->action_View_Collapse_All->setEnabled(hasItems);
    ui->action_View_Select_All->setEnabled(hasItems);
    ui->action_View_Select_None->setEnabled(hasItems);
    ui->action_View_Show_All->setEnabled(hasItems);
    ui->action_Size_Cols->setEnabled(hasItems);
    ui->action_Revert->setEnabled(hasSettingsFile);
    ui->action_Save_Settings->setEnabled(hasSettingsFile);
}

// Let multiple consumers get this from a single place, rather than N different panes.
void MainWindow::selectionChanged(const QItemSelectionModel* selector,
                                  const QItemSelection& selected, const QItemSelection& deselected)
{
    if (ui == nullptr)
        return;

    if (selector != nullptr) {
        const QAbstractItemModel* model = Util::MapDown(selector->model());

        if (dynamic_cast<const PointModel*>(model) != nullptr)
            emit selectedPointsChanged(selector, selected, deselected);

        if (dynamic_cast<const TrackModel*>(model) != nullptr)
            emit selectedTracksChanged(selector, selected, deselected);
    }

    updateActions();
}

// This is a funnel point for all panes to report changed events to.
// Consumers can then get the result from here, rather than N different places.
void MainWindow::currentChanged(const QModelIndex& current)
{
    const QModelIndex modelIdx = Util::MapDown(current); // emit in model space, not filter space

    if (modelIdx.model() == &trackModel()) {
        emit currentTrackChanged(modelIdx);
        emit currentTrackPointChanged(QModelIndex());

        // Update colorizer for new model.
        const PointModel* pointModel = static_cast<const TrackModel*>(modelIdx.model())->geoPoints(modelIdx);
        cfgData().pointColorizer.setModel(pointModel);
    }

    if (dynamic_cast<const PointModel*>(modelIdx.model()) != nullptr)
        emit currentTrackPointChanged(modelIdx);
}

void MainWindow::setupSignals()
{
    MainWindowBase::setupSignals();

    // Detect model changes so we can set dirty flags and auto-save if necessary
    for (Model mtype = Model::_First; mtype != Model::_Count; Util::inc(mtype)) {
        connect(&getModel(mtype), &QAbstractItemModel::dataChanged,  [this, mtype]() { dataChange(mtype, true); });
        connect(&getModel(mtype), &QAbstractItemModel::rowsInserted, [this, mtype]() { dataChange(mtype, true); });
        connect(&getModel(mtype), &QAbstractItemModel::rowsRemoved,  [this, mtype]() { dataChange(mtype, true); });
    }
}

void MainWindow::setupAutosave()
{
    if (cfgData().dataAutosaveInterval > 0) {
        autosaveTimer.setSingleShot(true);
        connect(&autosaveTimer, &QTimer::timeout, this, &MainWindow::saveModels, Qt::UniqueConnection);
    } else {
        disconnect(&autosaveTimer, &QTimer::timeout, this, &MainWindow::saveModels);
    }
}

void MainWindow::setStat(MainWindow::Stat s, const QString& txt, const QByteArray& fontStyle, const QColor& color)
{
    m_statusText[int(s)].setText(txt);

    m_statusText[int(s)].setStyleSheet("QLabel { font-style:" + fontStyle +
                                       "; color:" + color.name() + ";}");
}

void MainWindow::setStat(MainWindow::Stat s, const QString& txt, const QByteArray& fontStyle)
{
    setStat(s, txt, fontStyle, QGuiApplication::palette().color(QPalette::Text));
}

void MainWindow::updateStatus()
{
    const bool isOffline = ui->action_Offline_Mode->isChecked();

    setStat(Stat::Offline,  isOffline ? "[OFFLINE]" : "[ONLINE]", "normal",
            isOffline ? cfgData().uiColor[UiType::Warning] : cfgData().uiColor[UiType::Success]);
}

void MainWindow::updateStatus(const SelectionSummary& selectionSummary)
{
    updateStatus();

    setStat(Stat::Total,    QString("<b>Total:</b> %1").arg(selectionSummary.total));
    setStat(Stat::Visible,  QString("<b>Visible:</b> %1").arg(selectionSummary.visible));
    setStat(Stat::Selected, QString("<b>Selected:</b> %1").arg(selectionSummary.selected));
    setStat(Stat::Length,   QString("<b>Length:</b> %1").arg(cfgData().unitsTrkLength(selectionSummary.length)));
    setStat(Stat::Duration, QString("<b>Duration:</b> %1").arg(cfgData().unitsDuration(selectionSummary.duration)));
    setStat(Stat::Ascent,   QString("<b>Ascent:</b> %1").arg(cfgData().unitsClimb(selectionSummary.ascent)));
    setStat(Stat::Descent,  QString("<b>Descent:</b> %1").arg(cfgData().unitsClimb(selectionSummary.descent)));
}

void MainWindow::setGeoPositionStatus(const QString& posText)
{
    if (!statusBar()->isVisible())
        return;

    setStat(Stat::GeoLocation, posText);
}

// We don't directly set the progressBar values, because it's very slow, and sometimes we get
// update spam.  Instead, we update locally cached values, and set the progress bar on a timer.
void MainWindow::progressTimerUpdate()
{
    // Avoid udpate spam: clamp to 1/8th sec update frequency
    if (progressTimer.elapsed() < 125)
        return;

    if (progressBar.maximum() != m_progressMax)
        progressBar.setMaximum(m_progressMax);

    if (progressBar.value() != m_progressValue)
        progressBar.setValue(m_progressValue);

    progressTimer.start();
}

void MainWindow::finishProgress()
{
    if (m_progressMax == std::numeric_limits<int>::max())
        return;

    m_progressMax   = std::numeric_limits<int>::max();
    m_progressValue = 1;

    progressTimerUpdate();
    progressBar.reset();
    progressBar.setFormat(tr("Done"));
}

void MainWindow::initProgress(int total)
{
    if (total == 0)
        return finishProgress();

    if (m_progressMax == std::numeric_limits<int>::max()) {
        progressBar.reset();
        m_progressValue = 0;
        m_progressMax   = total;
        progressBar.setFormat(tr("%p%")); // TODO: accept as parameter

        progressTimer.start();
    } else {
        m_progressMax = total;
    }
}

void MainWindow::failedProgress(int failed)
{
    const QString failedMsg = tr("ERROR: %d task(s) failed").arg(failed);
    statusMessage(UiType::Error, failedMsg);
}

void MainWindow::updateProgress(int done, bool add)
{
    m_progressValue = done + (add ? m_progressValue : 0);

    if (m_progressValue >= m_progressMax) {
        finishProgress();
    } else {
        // The timer is to protect against progress bar spam.  For very rapid operations, the
        // mere act of calling setValue (which updates the GUI) can be more expensive than the
        // operation itself.
        progressTimerUpdate();
    }
}

QAction *MainWindow::getPaneAction(PaneAction cc) const
{
    if (ui == nullptr)
        return nullptr;

    switch (cc) {
    case PaneAction::ShowAll:             return ui->action_View_Show_All;
    case PaneAction::ExpandAll:           return ui->action_View_Expand_All;
    case PaneAction::CollapseAll:         return ui->action_View_Collapse_All;
    case PaneAction::SelectAll:           return ui->action_View_Select_All;
    case PaneAction::SelectNone:          return ui->action_View_Select_None;
    case PaneAction::ResizeToFit:         return ui->action_Size_Cols;
    case PaneAction::SetFiltersVisible:   return ui->action_Show_Filters;
    case PaneAction::CopySelected:        return ui->action_Copy_Selected;
    case PaneAction::ViewAsTree:          return ui->action_View_AsTree;
    case PaneAction::PaneClose:           return ui->action_Pane_Close;
    case PaneAction::PaneAdd:             return nullptr; // it has sub-menus
    case PaneAction::PaneReplace:         return nullptr; // it has sub-menus
    case PaneAction::PaneSplitH:          return ui->action_Pane_Split_Horizontal;
    case PaneAction::PaneSplitV:          return ui->action_Pane_Split_Vertical;
    case PaneAction::PaneLeft:            return ui->action_Pane_Left;
    case PaneAction::PaneRight:           return ui->action_Pane_Right;
    case PaneAction::PaneRowUp:           return ui->action_Pane_Group_Left;
    case PaneAction::PaneRowDown:         return ui->action_Pane_Group_Right;
    case PaneAction::PaneBalanceSiblings: return ui->action_Pane_Balance_Siblings;
    case PaneAction::PaneBalanceTab:      [[fallthrough]];
    case PaneAction::PaneAddTab:          [[fallthrough]];
    case PaneAction::PaneRenameTab:       [[fallthrough]];
    case PaneAction::PaneCloseTab:        return mainWindowTabs()->getPaneAction(cc);
    default:                              return nullptr;
    }
}

QAction* MainWindow::getMainAction(MainAction aa) const
{
    switch (aa) {
    case MainAction::ZoomToSelection:     return ui->action_Zoom_To_Selection;
    case MainAction::DeleteSelection:     return ui->action_Delete_Selection;
    case MainAction::SelectPerson:        return ui->action_Select_Person;
    default: return nullptr;
    }
}

// Restore status color, or warning/etc colors stick for status tips
void MainWindow::statusChanged(const QString& /*txt*/) const
{
    changeStatusBarColor(QPalette::Text);
}

Pane::Container* MainWindow::containerFactory() const
{
    return Pane::factory<Pane::Container>(PaneClass::Group, const_cast<MainWindow&>(*this));
}

void MainWindow::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

    MainWindowBase::saveUiConfig(settings, [&]() {
        // Ugly cost cast: TODO: improve.  saveModels() doesn't *actually* change anything, but it uses
        // a few non-const methods to e.g, set the window title, so this is a hack to allow that.
        const_cast<MainWindow&>(*this).saveModels();
        MemberSave(settings, importOpts);
        MemberSave(settings, exportOpts);
        MemberSave(settings, m_person);
        SL::Save(settings, "offlineMode", ui->action_Offline_Mode->isChecked());
    });
}

void MainWindow::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    MainWindowBase::loadUiConfig(settings, [&]() {
        loadModels();
        MemberLoad(settings, importOpts);
        MemberLoad(settings, exportOpts);
        MemberLoad(settings, m_person);
        ui->action_Offline_Mode->setChecked(SL::Load(settings, "offlineMode", false));
    });

    // Update power data for the selected person
    trackModel().setPerson(m_person);
}

// This is executed post-load after the application returns to the main event loop, i.e, after the
// UI becomes visible.  It can be used to do things that depend on the existence of the UI.
void MainWindow::postLoadHook()
{
    ui->action_Show_Statusbar->setChecked(statusBar()->isVisible());
    ui->action_Show_Toolbar->setChecked(ui->mainToolBar->isVisible());
    setOfflineMode(ui->action_Offline_Mode->isChecked());

    MainWindowBase::postLoadHook();
}

void MainWindow::triggerSaveIfDirty()
{
    // Set global modification flag to display in window title
    const bool anyDirty = std::any_of(m_modelDirty.begin(), m_modelDirty.end(), [](bool b) { return b; });
    markModified(anyDirty);

    if (anyDirty && cfgData().dataAutosaveInterval > 0)
        autosaveTimer.start(cfgData().dataAutosaveInterval * 1000); // dataAutosaveInterval is in S, timer in mS
}

// Whenever a model that we save is modified, this hook is called.
void MainWindow::dataChange(Model mtype, bool dirty)
{
    if (m_modelLoading)  // don't do during model loading.
        return;

    m_modelDirty[int(mtype)] = dirty;
    triggerSaveIfDirty();
}

void MainWindow::newConfig()
{
    // If there's no person for power estimation, select one if available.
    if (m_person.isEmpty() || !cfgData().people.contains(m_person))
        if (cfgData().people.rowCount() > 0)
            m_person = cfgData().people.data(cfgData().people.index(0, PersonModel::Name), Util::RawDataRole).toString();

    // Refresh model display in case units changed
    m_modelLoading = true;
    trackModel().setPerson(m_person);
    trackModel().refresh();
    m_modelLoading = false;

    // Refresh area dialog
    areaDialog.newConfig();

    // let panes update from new config
    runOnPanels([](PaneBase* pane) { pane->newConfig(); });

    // New autosave timer, and trigger save if there's dirty data and timer went from zero to non-zero
    setupAutosave();
    triggerSaveIfDirty();
}

void MainWindow::viewAsTree(bool asTree)
{
    runOnFocusPane(&PaneBase::viewAsTree, asTree);

    ui->action_View_AsTree->setChecked(asTree);
    resizeColumnsAllPanes();
}

void MainWindow::newFocus(QObject* focus)
{
    MainWindowBase::newFocus(focus);

    if (PaneBase* focusPane = focusedPane(focus); focusPane != nullptr)
        ui->action_View_AsTree->setChecked(focusPane->viewIsTree());

    updateActions();
    updateStatus();
}

// Return name of the file to store the given model.
QString MainWindow::modelDataFile(Model model) const
{
    if (!hasSettingsFile())
        return QString();

    const QFileInfo settingsInfo(currentSettingsFile());

    // dataAutosavePath can optionally override GPS data file path.  Otherwise, use settings file path.
    QString path = cfgData().dataAutosavePath.isEmpty() ? settingsInfo.path() : cfgData().dataAutosavePath;
    path.reserve(path.size() + 64);

    path += QDir::separator();
    path += settingsInfo.completeBaseName();

    switch (model) {
    case Model::Track:  path += ".track.ztgps";  break;
    case Model::View:   path += ".view.ztgps";   break;
    case Model::Filter: path += ".filter.ztgps"; break;
    default:                                     break;
    }

    return path;
}

TreeModel& MainWindow::getModel(Model mtype)
{
    switch (mtype) {
    case Model::Track:  return trackModel();
    case Model::View:   return viewModel();
    case Model::Filter: return filterModel();

    default: assert(0 && "Bad model type");
             return trackModel();
    }
}

const TreeModel& MainWindow::getModel(Model mtype) const
{
    return const_cast<const TreeModel&>(const_cast<MainWindow&>(*this).getModel(mtype));
}

void MainWindow::saveModels()
{
    if (!hasSettingsFile())
        return;

    const QString visited    = modelDataFile(Model::_Count);
    const bool isNewLocation = (visited != m_modelVisited);

    m_modelVisited = visited;

    for (Model mtype = Model::_First; mtype != Model::_Count; Util::inc(mtype)) {
        const TreeModel& model = getModel(mtype);

        if (!isModelDirty(mtype) && !isNewLocation)  // don't save models that aren't dirty
            continue;

        VersionedStream stream;
        const QString dataFile = modelDataFile(mtype);

        if (!backupFile(dataFile, cfgData().backupDataCount))
            error(tr("Unable to create backup file: ") + dataFile, tr("Backup Save Data"));

        const QFileInfo fileInfo(dataFile);
        QFile file(dataFile);

        // Create save directory
        if (!QDir::root().mkpath(fileInfo.absolutePath())) {
            error(tr("Unable to create save directory: ") + fileInfo.absolutePath(), tr("Save Data"));
            continue;
        }

        if (!stream.openWrite(file, NativeMagic + quint32(mtype), NativeVersion)) {
            error(stream.errorString() + ": " + file.fileName(), tr("Save Data"));
            continue;
        }

        model.save(stream);

        if (stream.error() != VersionedStream::NoError)
            error(stream.errorString() + ": " + file.fileName(), tr("Save Data"));
        else
            dataChange(mtype, false); // mark as no longer dirty after save
    }
}

void MainWindow::loadModels()
{
    if (!hasSettingsFile())
        return;

    m_modelVisited = modelDataFile(Model::_Count);

    for (Model mtype = Model::_First; mtype != Model::_Count; Util::inc(mtype)) {
        TreeModel& model = getModel(mtype);

        VersionedStream stream;
        QFile file(modelDataFile(mtype));

        // No error if no model data: it might be a new config.
        if (!file.exists())
            continue;

        if (!stream.openRead(file, NativeMagic + quint32(mtype), 0x1000, NativeVersion)) {
            error(stream.errorString() + ": " + file.fileName(), tr("Load Data"));
            continue;
        }

        m_modelLoading = true;
        model.load(stream);
        m_modelLoading = false;

        if (stream.error() != VersionedStream::NoError)
            error(stream.errorString() + ": " + file.fileName(), tr("Load Data"));
        else
            dataChange(mtype, false); // mark as no longer dirty after load
    }

    // Don't refresh the model here: that'll happen in MainWindow::load()
}

void MainWindow::on_action_Show_Filters_triggered(bool checked)
{
    runOnPanels([checked](PaneBase* pane) {
        pane->setFiltersVisible(checked);
    });
}

void MainWindow::on_action_Pane_Balance_Siblings_triggered()
{
    balanceSiblings();
}

// Replace pane with splitter containing pane, and new pane.
void MainWindow::on_action_Pane_Split_Horizontal_triggered()
{
    splitPane(focusedPaneWarn(), Qt::Horizontal);
}

// Replace pane with splitter containing pane, and new pane.
void MainWindow::on_action_Pane_Split_Vertical_triggered()
{
    splitPane(focusedPaneWarn(), Qt::Vertical);
}

void MainWindow::on_action_Copy_Selected_triggered()
{
    runOnFocusPane(&Pane::copySelected);
}

void MainWindow::on_action_View_Show_All_triggered()
{
    runOnFocusPane(&Pane::showAll);
}

void MainWindow::on_action_View_Expand_All_triggered()
{
    runOnFocusPane(&Pane::expandAll);
}

void MainWindow::on_action_View_Collapse_All_triggered()
{
    runOnFocusPane(&Pane::collapseAll);
}

void MainWindow::on_action_View_Select_All_triggered()
{
    runOnFocusPane(&Pane::selectAll);
}

void MainWindow::on_action_View_Select_None_triggered()
{
    runOnFocusPane(&Pane::selectNone);
}

void MainWindow::on_action_Pane_Close_triggered()
{
    removePane(focusedPaneWarn());
}

void MainWindow::on_action_Pane_Left_triggered()
{
    movePane(focusedPaneWarn(), -1);
}

void MainWindow::on_action_Pane_Right_triggered()
{
    movePane(focusedPaneWarn(), +1);
}

void MainWindow::on_action_Pane_Group_Left_triggered()
{
    movePaneParent(focusedPaneWarn(), -1);
}

void MainWindow::on_action_Pane_Group_Right_triggered()
{
    movePaneParent(focusedPaneWarn(), +1);
}

void MainWindow::on_action_Reset_Ui_triggered()
{
    if (cfgData().warnOnRevert)
        if (warningDialog(tr("Reset UI Configuration"), tr("Reset UI configuration to defaults?")) != QMessageBox::Ok)
            return;

    appConfig.reset();
    setupDefaultPanels();
    statusMessage(UiType::Info, tr("UI configuration reset."));
}

void MainWindow::on_action_Quit_without_Save_triggered()
{
    saveOnExit = false;
    close();
}

void MainWindow::on_action_Export_Track_triggered()
{
    QModelIndexList selections;
    if (const PaneBase* pane = focusedPane(); pane != nullptr)
        selections = pane->getSelections();

    // If the selections aren't from the track model, don't use them.
    if (!selections.empty() && selections[0].model() != &trackModel())
        selections.clear();

    GeoSave saver(*this, trackModel(),
                  exportOpts.exportFormat(),
                  exportOpts.writeFormatted(),
                  exportOpts.indentLevel(),
                  exportOpts.indentSpaces(),
                  selections);

    if (saver.save(exportOpts)) {
        statusMessage(UiType::Info, tr("Track export completed."));
    } else {
        if (!saver.errorString().isEmpty())
            error(saver.errorString(), tr("Export Tracks"));
        else
            statusMessage(UiType::Warning, tr("Canceled"));
    }
}

void MainWindow::importTracks(const QStringList& files)
{
    GeoLoad loader(*this, trackModel(), importOpts.tags(), importOpts.trackColor());

    if (loader.load(importOpts, files)) {
        // For convenience: select the newly imported itens.
        runOnPanels<TrackPane>([&loader](TrackPane* pane) {
            pane->select(loader.imported(),
                         QItemSelectionModel::Clear | QItemSelectionModel::Select | QItemSelectionModel::Rows);
        });

        postImport();

        // Ungainly bit of text concatenation ahead.
        statusMessage(UiType::Info, tr("Track import completed.") +
                      ((loader.duplicates() > 0) ? (tr("  Skipped ") + QString::number(loader.duplicates()) +
                                                    tr(" duplicates."))
                                                 : QString()));
    } else {
        if (!loader.errorString().isEmpty())
            error(loader.errorString(), tr("Import Tracks"));
        else
            statusMessage(UiType::Warning, tr("Canceled"));
    }
}

void MainWindow::postImport()
{
    runOnPanels([](PaneBase* pane) { pane->resizeToFit(200); });  // small delay to let panes update.
    runOnPanels<DataColumnPaneBase>([](PaneBase* pane) { pane->sort(); });
}

void MainWindow::importTracks()
{
    importTracks(importOpts.getImportFilenames(GeoLoad::trackFileFilter));
}

void MainWindow::showBoxSelectionDialog(const Marble::GeoDataLatLonBox& region)
{
    areaDialog.newRegion(region);
    areaDialog.show();
}

bool MainWindow::isOfflineMode() const
{
    return ui->action_Offline_Mode->isChecked();
}

void MainWindow::setOfflineMode(bool offline)
{
    runOnPanels<MapPane>([offline](MapPane* mapPane) { mapPane->setOfflineMode(offline); });
    updateStatus();
}

void MainWindow::on_action_Import_Track_triggered()
{
    importTracks();
}

void MainWindow::on_action_Open_Settings_triggered()
{
    openSettings();
}

void MainWindow::on_action_Save_Settings_As_triggered()
{
    saveSettingsAs();
}

void MainWindow::on_action_Save_Settings_triggered()
{
    saveSettings();
}

void MainWindow::on_action_Revert_triggered()
{
    revertSettings();
}

void MainWindow::on_action_Enlarge_Font_triggered()
{
    changeFontSize(1.1);
}

void MainWindow::on_action_Shrink_Font_triggered()
{
    changeFontSize(0.9);
}

void MainWindow::on_action_Size_Cols_triggered()
{
    resizeColumnsAllPanes();
}

void MainWindow::on_action_Reset_Font_triggered()
{
    setFontSize(startupFontSize);
}

void MainWindow::on_action_Zoom_To_Selection_triggered()
{
    PaneBase* pane = focusedPaneWarn();

    if (DataColumnPane* dcp = dynamic_cast<DataColumnPane*>(pane); dcp != nullptr)
        dcp->zoomToSelection();
}

void MainWindow::on_action_Delete_Selection_triggered()
{
    PaneBase* pane = focusedPaneWarn();

    if (DataColumnPane* dcp = dynamic_cast<DataColumnPane*>(pane); dcp != nullptr) {
        if (cfgData().warnOnRemove)
            if (warningDialog(tr("Remove Selection"),
                              tr("You are about to remove the active selections.  Proceed?")) != QMessageBox::Ok)
                return;

        dcp->deleteSelection();
    }
}

void MainWindow::on_action_Select_Person_triggered()
{
    personDialog.setValue(m_person);

    if (personDialog.exec() != QDialog::Accepted)
        return;

    trackModel().setPerson(m_person = personDialog.value());
}

void MainWindow::on_action_Import_from_Device_triggered()
{
    deviceDialog.show();
}

void MainWindow::on_action_New_Window_triggered()
{
    newWindowInteractive();
}

void MainWindow::on_action_Offline_Mode_triggered(bool checked)
{
    setOfflineMode(checked);
}

void MainWindow::on_action_Donate_triggered()
{
    aboutDialog.showTab(AboutDialog::Donate);
}

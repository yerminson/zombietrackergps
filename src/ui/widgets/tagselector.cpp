/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tagselector.h"
#include "ui_tagselector.h"

#include <algorithm>
#include <src/util/util.h>
#include <src/util/icons.h>

#include "src/core/cfgdata.h"

TagSelector::TagSelector(const CfgData& cfgData, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TagSelector),
    cfgData(cfgData),
    activeTags(cfgData),
    activeFilter(activeTags, TagModel::Name, Util::RawDataRole, this)
{
    ui->setupUi(this);

    setupActionIcons();
    setupTagSelectors();
    setupSignals();
    setupButtons();
}

TagSelector::~TagSelector()
{
    delete ui;
}

void TagSelector::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Left, "arrow-left");
    Icons::defaultIcon(ui->action_Right, "arrow-right");
    Icons::defaultIcon(ui->action_Up, "arrow-up");
    Icons::defaultIcon(ui->action_Down, "arrow-down");
}

void TagSelector::setupTagSelectors()
{
    QTreeView& availTagView = *ui->availableTags;
    QTreeView& trackTagView = *ui->trackTags;

    // This is really read-only, but the Qt API only accepts non-const pointers, so we cast the
    // constness away.
    activeFilter.setSourceModel(const_cast<TagModel*>(&cfgData.tags));

    availTagView.setModel(&activeFilter);
    trackTagView.setModel(&activeTags);

    availTagView.setIconSize(cfgData.iconSizeTag);
    trackTagView.setIconSize(cfgData.iconSizeTag);

    // Hide some columns
    for (const ModelType mt : { TagModel::CdA, TagModel::Weight, TagModel::RR }) {
        availTagView.setColumnHidden(mt, true);
        trackTagView.setColumnHidden(mt, true);
    }
}

void TagSelector::setupSignals()
{
    connect(ui->availableTags, &QTreeView::doubleClicked, this, &TagSelector::availDoubleClicked);
    connect(ui->trackTags,     &QTreeView::doubleClicked, this, &TagSelector::activeDoubleClicked);
}

void TagSelector::setupButtons()
{
    ui->tagLeft->setDefaultAction(ui->action_Left);
    ui->tagRight->setDefaultAction(ui->action_Right);
    ui->tagUp->setDefaultAction(ui->action_Up);
    ui->tagDown->setDefaultAction(ui->action_Down);
}

QStringList TagSelector::tags() const
{
    QStringList tags;
    tags.reserve(activeTags.rowCount());

    for (int row = 0; row < activeTags.rowCount(); ++row)
        tags.append(activeTags.data(activeTags.index(row, TagModel::Name),
                                    activeTags.keyRole()).toString());

    return tags;
}

void TagSelector::setTags(const QStringList& tags)
{
    activeTags.removeRows(0, activeTags.rowCount()); // remove old data

    // find index from source and insert them in order.
    for (const auto& tag : tags)
        if (QModelIndex srcIdx = cfgData.tags.keyIdx(tag); srcIdx.isValid())
            activeTags.insertRow(cfgData.tags, srcIdx, activeTags.rowCount());

    update();
}

void TagSelector::toActive()
{
    const QModelIndexList selection = Util::MapDown(ui->availableTags->selectionModel()->selectedRows());
    const QModelIndex currentIdxDst = ui->trackTags->selectionModel()->currentIndex();

    const QModelIndex insertParent = (currentIdxDst.isValid() ? activeTags.parent(currentIdxDst) : QModelIndex());
    int               insertPos    = (currentIdxDst.isValid() ? currentIdxDst.row() : activeTags.rowCount());

    for (const auto& idx : selection)
        if (idx.column() == 0)
            if (!activeTags.contains(cfgData.tags.data(TagModel::Name, idx)) && !activeTags.isCategory(idx))
                activeTags.insertRow(cfgData.tags, idx, insertPos++, insertParent);

    // For convenience, select them.
    QItemSelectionModel* selectModel = ui->trackTags->selectionModel();
    selectModel->reset(); // don't emit selection changed

    for (const auto& idx : selection)
        if (idx.column() == 0)
            if (const QModelIndex trackIdx = activeTags.keyIdx(cfgData.tags.data(TagModel::Name, idx));
                    trackIdx.isValid())
                selectModel->select(trackIdx, QItemSelectionModel::Select | QItemSelectionModel::Rows);

    update();
}

void TagSelector::toAvail()
{
    activeTags.removeRows(ui->trackTags->selectionModel());

    update();
}

void TagSelector::setVisible(bool visible)
{
    if (visible) {
        ui->availableTags->expandAll();
        update();
    }

    QWidget::setVisible(visible);
}

void TagSelector::update()
{
    activeFilter.invalidate(); // invalidate filter

    Util::ResizeViewForData(*ui->trackTags);
    Util::ResizeViewForData(*ui->availableTags);
}

void TagSelector::moveSelections(int rel)
{
    QItemSelectionModel* selectModel = ui->trackTags->selectionModel();

    const QModelIndexList selection = selectModel->selectedRows();
    QList<QPersistentModelIndex> sorted;
    QPersistentModelIndex current = selectModel->currentIndex();

    sorted.reserve(selection.size());
    for (const auto& idx : selection)
        sorted.append(idx);

    // Chose order depending on whether it's moving up or down.  Otherwise we step on ones
    // we've already moved.
    std::sort(sorted.begin(), sorted.end(), [&rel](const QModelIndex& l, const QModelIndex& r) {
        return (rel < 0) ? (l.row() < r.row()) : (l.row() > r.row());
    });

    for (const auto& idx : sorted) {
        const QModelIndex parent = activeTags.parent(idx);
        const int newRow = idx.row() + rel;
        // Moving down is increment of 2: see comment in docs for QAbstractItemModel::beginMoveRows.
        if (newRow >= 0 && newRow <= (activeTags.rowCount(parent)+1))
            activeTags.moveRow(parent, idx.row(), parent, newRow);
    }

    selectModel->reset(); // don't emit selection changed
    selectModel->setCurrentIndex(current, QItemSelectionModel::Current | QItemSelectionModel::Rows);

    for (const auto& idx : sorted)
        selectModel->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
}

void TagSelector::activeDoubleClicked()
{
    toAvail();
}

void TagSelector::availDoubleClicked()
{
    toActive();
}

void TagSelector::on_action_Left_triggered()
{
    toActive();
}

void TagSelector::on_action_Right_triggered()
{
    toAvail();
}

void TagSelector::on_action_Up_triggered()
{
    moveSelections(-1);
}

void TagSelector::on_action_Down_triggered()
{
    // Moving down is 2: see comment in docs for QAbstractItemModel::beginMoveRows.
    moveSelections(2);
}

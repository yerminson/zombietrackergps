/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKMAP_H
#define TRACKMAP_H

#include <QPersistentModelIndex>
#include <QPixmap>
#include <QTimer>
#include <QPoint>
#include <marble/MarbleWidget.h>
#include <marble/GeoDataCoordinates.h>

#include <src/util/roles.h>

namespace Marble {
class GeoPainter;
}

class MainWindow;
class CfgData;
class PointItem;
class TrackModel;
class QMouseEvent;

// This class inherits from MarbleWidget to paint some data on top of the map,
// where otherwise we would need a prohibitive number of Placemarks.  Allocating
// all those on the fly is very slow.  This is a higher performance way to draw
// icons on track points, for example.
class TrackMap final : public Marble::MarbleWidget
{
public:
    TrackMap(MainWindow&, TrackModel&);

    void newConfig();

    TrackModel& trackModel() { return m_trackModel; }
    const TrackModel& trackModel() const { return m_trackModel; }

    void setOfflineMode(bool offline);

    void startSelectRegion(const QPoint&);
    void endSelectRegion(const QPoint&);

protected:
    void customPaint(Marble::GeoPainter* painter) override;

private slots:
    void currentTrackChanged(const QModelIndex&);
    void currentPointChanged(const QModelIndex&);
    void visibleTracksChanged();
    void selectedPointChanged();
    void newViewContext(Marble::ViewContext newViewContext);
    void processRegionSelected(const QList<double>&);
    void hqUpdate();
    void handleProgress(int active, int queued);
    void handleJobRemoved();

private:
    MainWindow& mainWindow() { return m_mainWindow; }
    const MainWindow& mainWindow() const { return m_mainWindow; }

    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;

    void resizeEvent(QResizeEvent *event) override; // low quality during resize events

    template <typename T> T get(int mt, const QModelIndex& idx, int role = Util::RawDataRole) const;
    inline void drawSinglePt(Marble::GeoPainter*, const PointItem&, qreal threshold, const QPixmap&,
                             bool draw, bool force);
    void drawSingleTrack(Marble::GeoPainter*, const QModelIndex&);
    void drawTrackPts(Marble::GeoPainter*);
    void drawTrackLines(Marble::GeoPainter*);
    void drawSelectionBox(Marble::GeoPainter*);
    inline static void drawTrackSeg(Marble::GeoPainter*, const QColor&, float trackWidth,
                             const TrackSegLines&);
    bool isCurrent(const QModelIndex& idx) const { return currentTrackIdx == idx; }

    void setupIcons();
    void setupSignals();
    void setupTimers();
    void deferredUpdate();
    void avoidSomeRepaints(Marble::GeoPainter* painter);

    Marble::GeoDataCoordinates widgetGeoCoords(const QPoint&);

//  MarbleWidget::regionSelected (for some reason!) has different definitions between
//  the same apparent version (libmarblewidget-qt5.so.28) between different flavors of
//  Linux.  Unclear why, but it means we can't use that symbol from the shared library
//  and successfully run in all environments.

//  To work around that, we draw our own selection box, which is very awkward in the
//  MarbleWidget API, and doesn't erase itself correctly in all situations :(.

//  Ubuntu and Debian, from objdump -C -T <lib> | grep -i regionSelect:
//    Marble::MarbleWidget::regionSelected(QList<double> const&)
//    Marble::MarbleAbstractPresenter::regionSelected(QList<double> const&)

//  OpenSUSE, same library version:
//    Marble::MarbleAbstractPresenter::regionSelected(Marble::GeoDataLatLonBox const&)
//    Marble::MarbleWidget::regionSelected(Marble::GeoDataLatLonBox const&)

    bool regionSelectionStarted() const { return !selectionStartCoordinate.isNull(); }
    bool regionSelectionInProgress() const { return !selectionStartCoordinate.isNull() && !selectionEndCoordinate.isNull(); }

    const CfgData& cfgData() const;

    MainWindow&           m_mainWindow;
    TrackModel&           m_trackModel;

    QPoint                selectionStartCoordinate;
    QPoint                selectionEndCoordinate;

    // for tracking which track we've set to active.  This is here because setting
    // the track as active may change it in multiple map views, which all share the
    // same GeoDataStyle pointer, which we want to change once, not once per map.
    QPersistentModelIndex currentTrackIdx;     // reflect last current track
    QPersistentModelIndex currentPointIdx;     // reflect last current point
    QPixmap               defaultPointIcon;    // for default track points
    QPixmap               selectedPointIcon;   // for selected track points
    QPixmap               currentPointIcon;    // for current track points
    QTimer                hqUpdateTimer;       // high quality update

    qreal                 prevX;  // for point proximity culling
    qreal                 prevY;  // ...
};

#endif // TRACKMAP_H
